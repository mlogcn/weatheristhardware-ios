//
//  DeviceManagerViewController.m
//  WeatherLog
//
//  Created by ink on 15/8/26.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import "DeviceManagerViewController.h"
#define COLOR(a,b,c,d) [UIColor colorWithRed:a/255.0 green:b/255.0 blue:c/255.0 alpha:d]

@interface DeviceManagerViewController ()

@end

@implementation DeviceManagerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithRed:61 / 255.0 green:198 / 255.0 blue:182 / 255.0 alpha:1.0];
    persentView  = [[PercentView alloc] initWithFrame:CGRectMake(0, 0, 145, 145)];
    persentView.center = CGPointMake(self.view.centerx, self.view.centery - 50);
    [persentView setPrecent:self.batteryNum.intValue description:@"电池电量" textColor:[UIColor orangeColor] bgColor:COLOR(0, 0, 0, 0) alpha:1 clips:NO];
    persentView.endless = YES;
    [self.view addSubview:persentView];
    
    UIButton * button = [UIButton new];
    [button setTitle:@"解除绑定" forState:UIControlStateNormal];
    button.layer.masksToBounds = YES;
    button.layer.cornerRadius = 10;
    button.frame = CGRectMake(0, 0, 150, 75);
    button.center = CGPointMake(self.view.centerx, CGRectGetHeight(self.view.frame) - 100);
    button.backgroundColor = COLOR(75, 0, 130, 1);
    [button addTarget:self action:@selector(clearDevice) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    UIButton *button1=[[UIButton alloc]init];
    [button1 setTitle:@"返回" forState:UIControlStateNormal];
    button1.translatesAutoresizingMaskIntoConstraints=NO;
    [button1 setBackgroundColor:[UIColor blackColor]];
    [self.view addSubview:button1];
    NSArray * constraints3 = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[button1(==70)]"
                                                                     options:0
                                                                     metrics:nil
                                                                       views:NSDictionaryOfVariableBindings(button1)];
    NSArray * constraints4 = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-50-[button1(==30)]"
                                                                     options:0
                                                                     metrics:nil
                                                                       views:NSDictionaryOfVariableBindings(button1)];
    [self.view addConstraints:constraints3];
    [self.view addConstraints:constraints4];
    [button1 addTarget:self action:@selector(disConnect) forControlEvents:UIControlEventTouchUpInside];
    button1.layer.masksToBounds = YES;
    button1.layer.cornerRadius = 5;

}
- (void)viewDidAppear:(BOOL)animated{
    [persentView addAnimateWithType:1];

}

- (void)clearDevice{
    [self.weatherLog disconnect];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"UUID"];
    [self dismissViewControllerAnimated:YES completion:^{
        [self.delegate clearConnect];
    }];
}
- (void)disConnect{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
