//
//  RootViewController.h
//  WeatherLog
//
//  Created by ink on 15/8/5.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WLCentralManager.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import "ScanViewController.h"
#import "WLWeatherLog.h"
#import "WLBarometerService.h"
#import "WLHumidityService.h"
#import "WLUVIndexService.h"
#import "WLBatteryService.h"
#import <Masonry.h>
#import "DailView.h"
#import "LineView.h"
#import "RootViewModel.h"
#import "WLMonitorService.h"
#import "DeviceManagerViewController.h"
#import "SetTextWithWeather.h"
@interface RootViewController : UIViewController<CBPeripheralDelegate,CBCentralManagerDelegate,ScanDelegate,DailDelegate,DisConnectDelegate>{
    UIScrollView * contentScrollView;
    DailView * _dailView;
    LineView * _weatherLineView;
    WLWeatherLog * deviceYP;
    BOOL loadSuc;
    NSInteger countNum;
    SetTextWithWeather * ppp;
    
}
//@property (strong, nonatomic) WLBarometerService *barometerService;
//@property (strong, nonatomic) WLHumidityService *humidityService;
//@property (strong, nonatomic) WLUVIndexService *uvIndexService;
@property (strong, nonatomic) WLBatteryService *batteryService;
@property (strong, nonatomic) WLMonitorService * monitorService;
@end
