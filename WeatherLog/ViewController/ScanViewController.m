//
//  ScanViewController.m
//  WeatherLog
//
//  Created by ink on 15/8/19.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import "ScanViewController.h"
#define ScreenHeigth [UIScreen mainScreen].bounds.size.height
#define ScreenWidth [UIScreen mainScreen].bounds.size.width
#define ServiceName @"WeatherLog"
#define ServiceName1 @"Thermometer Sensor"
@interface ScanViewController ()

@end

@implementation ScanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.view.backgroundColor = [UIColor redColor];
    isFind = NO;
    UIImage * image = [UIImage imageNamed:@"IPhone_5s.png"];
    UIImageView * imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 100,100)];
    imageView.image = image;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.center = CGPointMake(self.view.center.x, 150);
    [self.view addSubview:imageView];
    PulsingHaloLayer *layer = [PulsingHaloLayer layer];
    layer.radius = 100;
    self.halo = layer;
    self.halo.position = imageView.center;
    [self.view.layer insertSublayer:self.halo below:imageView.layer];


    WLCentralManager * manager = [WLCentralManager sharedService];
    manager.delegate = self;
    UIButton *button=[[UIButton alloc]init];
    [button setTitle:@"绑定设备" forState:UIControlStateNormal];
    button.translatesAutoresizingMaskIntoConstraints=NO;
    [button setBackgroundColor:[UIColor blackColor]];
    [self.view addSubview:button];
    NSArray * constraints1 = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[button]-|"
                                                                     options:0
                                                                     metrics:nil
                                                                       views:NSDictionaryOfVariableBindings(button)];
    NSArray * constraints2 = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[button(==30)]-100-|"
                                                                     options:0
                                                                     metrics:nil
                                                                       views:NSDictionaryOfVariableBindings(button)];
    [self.view addConstraints:constraints1];
    [self.view addConstraints:constraints2];
    [button addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton *button1=[[UIButton alloc]init];
    [button1 setTitle:@"返回" forState:UIControlStateNormal];
    button1.translatesAutoresizingMaskIntoConstraints=NO;
    [button1 setBackgroundColor:[UIColor blackColor]];
    [self.view addSubview:button1];
    NSArray * constraints3 = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[button1(==70)]"
                                                                     options:0
                                                                     metrics:nil
                                                                       views:NSDictionaryOfVariableBindings(button1)];
    NSArray * constraints4 = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-50-[button1(==30)]"
                                                                     options:0
                                                                     metrics:nil
                                                                       views:NSDictionaryOfVariableBindings(button1)];
    [self.view addConstraints:constraints3];
    [self.view addConstraints:constraints4];
    [button1 addTarget:self action:@selector(disConnect) forControlEvents:UIControlEventTouchUpInside];

}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    WLCentralManager * manager = [WLCentralManager sharedService];
    [manager startScan];
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central{
    
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI{
    
    if ([peripheral.name isEqualToString:ServiceName] || [peripheral.name isEqualToString:ServiceName1]) {
        if (!isFind) {
                [SVProgressHUD show];
            [SVProgressHUD showSuccessWithStatus:@"发现设备"];
            WLCentralManager * manager = [WLCentralManager sharedService];
            [manager stopScan];
            YMSCBPeripheral * yp = [manager findPeripheral:peripheral];
            weatherLog = (WLWeatherLog *)yp;
            [self.halo stopScan];
            [[NSUserDefaults standardUserDefaults] setObject:weatherLog.cbPeripheral.identifier.UUIDString forKey:@"UUID"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSLog(@"==========%@",yp);
            isFind = YES;
        }
    }
}

- (void)dismiss{
    [self dismissViewControllerAnimated:YES completion:^{
        [self.delegate getServiceWithPeripheral:weatherLog];
    }];

}

- (void)disConnect{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
