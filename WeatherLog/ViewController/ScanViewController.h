//
//  ScanViewController.h
//  WeatherLog
//
//  Created by ink on 15/8/19.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "WLCentralManager.h"
#import "WLWeatherLog.h"
#import "PulsingHaloLayer.h"
#import <SVProgressHUD.h>
@protocol ScanDelegate
- (void)getServiceWithPeripheral:(WLWeatherLog *)Peripheral;
@end

@interface ScanViewController : UIViewController<CBCentralManagerDelegate>{
    WLWeatherLog * weatherLog;
    BOOL isFind;
}
@property (nonatomic, assign) id<ScanDelegate>delegate;
@property (nonatomic, weak) PulsingHaloLayer *halo;


@end
