//
//  DeviceManagerViewController.h
//  WeatherLog
//
//  Created by ink on 15/8/26.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PercentView.h"
#import "WLWeatherLog.h"
@protocol DisConnectDelegate <NSObject>

- (void)clearConnect;

@end



@interface DeviceManagerViewController : UIViewController{
    PercentView * persentView;
}

@property (nonatomic, strong) NSNumber * batteryNum;

@property (nonatomic, strong) WLWeatherLog * weatherLog;

@property (nonatomic ,assign) id<DisConnectDelegate>delegate;

@end
