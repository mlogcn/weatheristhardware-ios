//
//  RootViewController.m
//  WeatherLog
//
//  Created by ink on 15/8/5.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import "RootViewController.h"
#define ScreenHeigth [UIScreen mainScreen].bounds.size.height
#define ScreenWidth [UIScreen mainScreen].bounds.size.width

@interface RootViewController ()
@property (nonatomic) RootViewModel * viewModel;
@end

@implementation RootViewController
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    WLCentralManager * manager = [WLCentralManager sharedService];
    manager.delegate = self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.viewModel = [RootViewModel new];
    [self addContent];
    ppp = [SetTextWithWeather new];
    loadSuc = [ppp LoadTheXML];
   
//    NSLog(@"%@",[WLHandleWeatherData GetTheWeatherDataByName:@"sen_temp" Duration:[NSNumber numberWithInteger:1] Offset:[NSNumber numberWithInteger:0]]);
}
- (void)addContent{
    UIImageView * imageView = [[UIImageView alloc] initWithFrame:self.view.frame];
    imageView.image = [UIImage imageNamed:@"bg.png"];
    [self.view addSubview:imageView];
    contentScrollView = [UIScrollView new];
    contentScrollView.pagingEnabled = YES;
    [self.view addSubview:contentScrollView];
    [contentScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    // Do any additional setup after loading the view.
//    self.title = @"这特么是硬件,biubiubiu";
    self.navigationController.navigationBarHidden = YES;
    [WLCentralManager initSharedServiceWithDelegate:self];
    UIView * contentView = [UIView new];
    [contentScrollView addSubview:contentView];
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(contentScrollView);
        make.width.equalTo(contentScrollView);
    }];
    _dailView = [DailView new];
    [contentView addSubview:_dailView];
    [_dailView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(contentView.mas_top);
        make.left.equalTo(contentView.mas_left);
        make.width.mas_equalTo(ScreenWidth);
        make.height.mas_equalTo(ScreenHeigth);
        
    }];
    NSLog(@"%f,%f",ScreenHeigth,self.view.frame.size.height);
    _dailView.delegate = self;
    
    _weatherLineView = [LineView new];
    [contentView addSubview:_weatherLineView];
    [_weatherLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_dailView.mas_bottom);
        make.left.equalTo(contentView.mas_left);
        make.width.mas_equalTo(ScreenWidth);
        make.height.mas_equalTo(ScreenHeigth);
        
    }];

    
        self.view.backgroundColor = [UIColor colorWithRed:39 / 255.0 green:64 / 255.0 blue:139 / 255.0 alpha:1.0];
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(_weatherLineView.mas_bottom);
    }];

    [[self.viewModel fetchNetData] subscribeNext:^(NSArray * array) {
        _weatherLineView.weatherDataArray = array;
    }];
    
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central{
    if (central.state == CBCentralManagerStatePoweredOn) {
        // Scan for devices
        NSUUID * uuid = [[NSUUID alloc] initWithUUIDString:[[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"]];
        if (uuid != nil) {
            [self retrievePeripheralWithUUIDandConnect:uuid];
        }
    }
}

- (void)centralManager:(CBCentralManager *)central didRetrievePeripherals:(NSArray *)peripherals{
    
    NSLog(@"retrieve peripherals=====%@",peripherals);
    if (peripherals.count > 0) {
        
        CBPeripheral * per = peripherals[0];
       deviceYP = (WLWeatherLog *)[[WLCentralManager sharedService] findPeripheral:per];
        [deviceYP disconnect];
        deviceYP.delegate = self;
        [deviceYP connect];
    }
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral{
    WLCentralManager * manager = [WLCentralManager sharedService];
    YMSCBPeripheral * yp  = [manager findPeripheral:peripheral];
    yp.delegate = self;
    WLWeatherLog * weatherLog = (WLWeatherLog *)yp;

//    self.barometerService = weatherLog.serviceDict[@"barometer"];
//    self.humidityService = weatherLog.serviceDict[@"humidity"];
//    self.uvIndexService =  weatherLog.serviceDict[@"uvindex"];
    self.batteryService = weatherLog.serviceDict[@"battery"];
//    [self.humidityService addObserver:self forKeyPath:@"relativeHumidity" options:NSKeyValueObservingOptionNew context:NULL];
    [self.batteryService addObserver:self forKeyPath:@"batteryLevel" options:NSKeyValueObservingOptionNew context:NULL];
//    [self.barometerService addObserver:self forKeyPath:@"pressure" options:NSKeyValueObservingOptionNew context:NULL];
//    [self.uvIndexService addObserver:self forKeyPath:@"UVI" options:NSKeyValueObservingOptionNew context:NULL];
    self.monitorService = weatherLog.serviceDict[@"monitor"];
    [self.monitorService setSpeed:12.0];
    [self.monitorService addObserver:self forKeyPath:@"sensorValues" options:NSKeyValueObservingOptionNew context:nil];
    NSTimer * timer = [NSTimer timerWithTimeInterval:2 target:self selector:@selector(getWeatherValue) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];

//
//    NSLog(@"%@",yp.name);
 
}
- (void)getWeatherValue{
//    NSLog(@"00000000000000000%@,%@,%@,%@",self.humidityService.ambientTemp,self.humidityService.relativeHumidity,self.uvIndexService.UVI,self.barometerService.pressure);
}
- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error{
    NSLog(@"%@",error);
}
/**
 *  设备管理
 */
- (void)deviceManager{
    
}
- (void)scanBlue:(id)sender{

}
- (void)getServiceWithPeripheral:(WLWeatherLog *)Peripheral{
    NSLog(@"-=-=-=-=%@",Peripheral);
    deviceYP = Peripheral;
    Peripheral.delegate = self;
    [Peripheral connect];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
      if (object == self.monitorService) {
          
        WLMonitorService * monitorService = (WLMonitorService *)object;
        double pressure = [[monitorService.sensorValues objectForKey:@"pressure"] doubleValue] / 100;
        if (pressure < 980) {
            pressure = 980;
        }else if (pressure > 1040){
            pressure = 1040;
        }
        double moveValue = ( pressure - 980) / (1040 - 980) * 100;
        [_dailView changeValueWithDeviceTag:1 AndValue:moveValue];
        [_dailView changeValueWithDeviceTag:2 AndValue: [[monitorService.sensorValues objectForKey:@"UVI"] doubleValue]];
        //[_dailView changeValueWithDeviceTag:3 AndValue:monitorService.ambientTemp.doubleValue];
        [_dailView changeTempAndHum:[monitorService.sensorValues objectForKey:@"ambientTemp"] humidity:[monitorService.sensorValues objectForKey:@"relativeHumidity"]];
          
          if (loadSuc) {
              if (countNum == 10) {
                  countNum=0;
                  NSArray * resultArray =   [ppp ParseTheXML];
                  switch (resultArray.count) {
                      case 1:{
                          WLCriterion * criterion =resultArray[0];
                          if (criterion != nil) {
                              NSString * notificationString = criterion.notification;
                              //                      [notificationString stringByReplacingOccurrencesOfString:@"\n" withString:@"，"];
                              [_dailView setNotifiyText:notificationString];
                          }

                          break;
                      }
                      case 2:{
                          WLCriterion * criterion =resultArray[0];
                          if (criterion != nil) {
                              NSString * notificationString = criterion.notification;
                              //                      [notificationString stringByReplacingOccurrencesOfString:@"\n" withString:@"，"];
                              [_dailView setNotifiyText:notificationString];
                          }
                          WLCriterion * criterion1 =resultArray[01];
                          if (criterion1 != nil) {
                              NSString * notificationString = criterion1.notification;
                              //                      [notificationString stringByReplacingOccurrencesOfString:@"\n" withString:@"，"];
                              [_dailView setOtherNotifiyText:notificationString];
                          }
                        
                      }
                          
                      default:
                          break;
                  }
                  if (resultArray.count > 0) {
                      
                  }
              }else{
                  countNum++;

              }
          }else{
              loadSuc = [ppp LoadTheXML];
              
          }
    }
//    if (object == self.barometerService) {
//        WLBarometerService * barometerServiceTmp = (WLBarometerService *)object;
//        double pressure = barometerServiceTmp.pressure.doubleValue / 100;
//        NSLog(@"============%f",barometerServiceTmp.pressure.doubleValue / 100);
//        if (pressure < 980) {
//            pressure = 980;
//        }else if (pressure > 1040){
//            pressure = 1040;
//        }
//        double moveValue = ( pressure - 950) / (1040 - 950) * 100;
//        [_dailView changeValueWithDeviceTag:1 AndValue:moveValue];
//
//    }else if (object == self.humidityService){
//        WLHumidityService * humidityServiceTmp = (WLHumidityService *)object;
//        NSLog(@"------------%@",humidityServiceTmp.relativeHumidity);
//        [_dailView changeTempAndHum:humidityServiceTmp];
//
//
//
//    }else if (object == self.uvIndexService){
//        WLUVIndexService * uvIndexServiceTmp = (WLUVIndexService *)object;
//        [_dailView changeValueWithDeviceTag:2 AndValue:uvIndexServiceTmp.UVI.doubleValue];
//        NSLog(@"++++++++++++%@",uvIndexServiceTmp.UVI);
////
//        
//    }
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI{
    NSLog(@"Discovered %@ at %@", peripheral.name, RSSI);

}
- (void)buttonSelectWithIndex:(NSInteger)index{
    switch (index) {
        case 1:{
            if ([[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"] == nil) {
                if (self.batteryService != nil) {
                    
                    DeviceManagerViewController * deviceManagerViewController = [DeviceManagerViewController new];
                    deviceManagerViewController.batteryNum = self.batteryService.batteryLevel;
                    deviceManagerViewController.weatherLog = deviceYP;
                    deviceManagerViewController.delegate = self;
                    [self.navigationController presentViewController:deviceManagerViewController animated:YES completion:nil];
                    
                }else{
                    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请先连接设备" delegate:nil cancelButtonTitle:@"确认" otherButtonTitles:nil, nil];
                    [alertView show];
                }
                

            }
            
            
            if (deviceYP != nil) {
                
                UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"设备已连接，请先删除设备" delegate:nil cancelButtonTitle:@"确认" otherButtonTitles:nil, nil];
                [alertView show];
            }else{
                ScanViewController * scanViewController = [ScanViewController new];
                scanViewController.delegate = self;
                [self presentViewController:scanViewController animated:YES completion:nil];
 
            }
            

            break;
        }
        case 2:{
            if ([[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"] == nil) {
                ScanViewController * scanViewController = [ScanViewController new];
                scanViewController.delegate = self;
                [self presentViewController:scanViewController animated:YES completion:nil];
                break;
            
            }else{
                
//                if (self.batteryService != nil) {
                
                    DeviceManagerViewController * deviceManagerViewController = [DeviceManagerViewController new];
                    deviceManagerViewController.batteryNum = self.batteryService.batteryLevel;
                    deviceManagerViewController.weatherLog = deviceYP;
                    deviceManagerViewController.delegate = self;
                    [self.navigationController presentViewController:deviceManagerViewController animated:YES completion:nil];
                    
//                }else{
//                    UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请先连接设备" delegate:nil cancelButtonTitle:@"确认" otherButtonTitles:nil, nil];
//                    [alertView show];
//                }
                break;
            }
          
        }
        case 3:{
            [contentScrollView setContentInset:UIEdgeInsetsZero];
            NSUUID * uuid = [[NSUUID alloc] initWithUUIDString:[[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"]];
            if (uuid != nil) {
                [self retrievePeripheralWithUUIDandConnect:uuid];
            }

            break;
        }
            
        default:
            break;
    }
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error{
    NSLog(@"disconnect,%@",error);
    NSUUID * uuid = [[NSUUID alloc] initWithUUIDString:[[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"]];
    if (uuid != nil) {
        [self retrievePeripheralWithUUIDandConnect:uuid];
    }

}

- (void) retrievePeripheralWithUUIDandConnect:(NSUUID *)uuid{
    NSArray *results = [[WLCentralManager sharedService] retrievePeripheralsWithIdentifiers:@[uuid]];
    if (results.count > 0) {
        for (CBPeripheral *peripheral in results) {
            [[WLCentralManager sharedService] handleFoundPeripheral:peripheral];
        }
    }
    CBCentralManager *central = (CBCentralManager *) [WLCentralManager sharedService];
    [self centralManager:central didRetrievePeripherals:results];
}
- (void)clearConnect{
    _batteryService = nil;
    _monitorService = nil;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
