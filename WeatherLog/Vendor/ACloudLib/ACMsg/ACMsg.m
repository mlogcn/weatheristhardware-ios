//
//  ACMsg.m
//  ACloudLib
//
//  Created by zhourx5211 on 12/10/14.
//  Copyright (c) 2014 zcloud. All rights reserved.
//

#import "ACMsg.h"

@implementation ACMsg

NSString *const ACMsgObjectPayload = @"application/x-zc-object";
NSString *const ACMsgJsonPayload = @"text/json";
NSString *const ACMsgStreamPayload = @"application/octet-stream";
NSString *const ACMsgMsgNameHeader = @"X-Zc-Msg-Name";
NSString *const ACMsgAckMSG = @"X-Zc-Ack";
NSString *const ACMsgErrMSG = @"X-Zc-Err";

- (void)setPayload:(NSData *)payload format:(NSString *)format
{
    _payload = payload;
    _payloadFormat = format;
    _payloadSize = payload.length;
}

- (void)setStreamPayload:(NSData *)streamPayload size:(NSInteger)size
{
    if (size <= 0) {
        return;
    }
    _payloadFormat = ACMsgStreamPayload;
    _streamPayload = streamPayload;
    _payloadSize = size;
}

- (void)setErr:(NSInteger)errCode errMsg:(NSString *)errMsg
{
    self.name = ACMsgErrMSG;
    [self putInteger:@"errorCode" value:errCode];
    [self putString:@"error" value:errMsg];
}

- (BOOL)isErr
{
    return ![self.name isEqualToString:ACMsgAckMSG];
}

- (NSInteger)getErrCode
{
    return [self getInteger:@"errorCode"];
}

- (NSString *)getErrMsg
{
    return [self getString:@"error"];
}

- (void)setAck
{
    self.name = ACMsgAckMSG;
}

@end
