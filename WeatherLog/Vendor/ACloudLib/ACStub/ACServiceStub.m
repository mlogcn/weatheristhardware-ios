//
//  ACServiceStub.m
//  AbleCloud
//
//  Created by zhourx5211 on 1/15/15.
//  Copyright (c) 2015 ACloud. All rights reserved.
//

#import "ACServiceStub.h"

@interface ACServiceStub ()

@property (strong, nonatomic) NSMutableDictionary *serviceStubs;

@end

@implementation ACServiceStub

- (id)init
{
    self = [super init];
    if (self) {
        self.serviceStubs = [[NSMutableDictionary alloc] init];
    }
    return self;
}

+ (instancetype)sharedInstance
{
    static id instance;
    static dispatch_once_t predicate;
    
    dispatch_once(&predicate, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}

+ (void)setServiceStub:(NSString *)serviceName delegate:(id<ACServiceStubDelegate>)delegate
{
    if (serviceName && delegate) {
        [[ACServiceStub sharedInstance].serviceStubs setObject:delegate forKey:serviceName];
    }
}

+ (id<ACServiceStubDelegate>)getServiceStubDelegate:(NSString *)serviceName
{
    if (serviceName) {
        return [[ACServiceStub sharedInstance].serviceStubs objectForKey:serviceName];
    }
    return nil;
}

+ (void)removeServiceStub:(NSString *)serviceName
{
    [[ACServiceStub sharedInstance].serviceStubs removeObjectForKey:serviceName];
}

+ (BOOL)isServiceStub:(NSString *)serviceName
{
    return [[ACServiceStub sharedInstance].serviceStubs.allKeys containsObject:serviceName];
}

@end
