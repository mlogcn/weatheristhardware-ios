//
//  ACloudLib.m
//  ACloudLib
//
//  Created by zhourx5211 on 14/12/8.
//  Copyright (c) 2014年 zcloud. All rights reserved.
//

#import "ACloudLib.h"
#import "ACServiceClient.h"

#define BASE_URL_STRING  @"http://test.ablecloud.cn:5000"

#define MAJOR_DOMAIN  @"test"
#define MAJOR_DOMAIN_ID  11
#define TIMEOUT  @"600"

#define DEVICE_METHOD_SEND @"sendToDevice"

@interface ACloudLib ()

@property (strong, nonatomic) NSString *host;
@property (strong, nonatomic) NSString *stubHost;
@property (strong, nonatomic) NSString *majorDomain;
@property (assign, nonatomic) NSInteger majorDomainId;
@property (strong, nonatomic) NSString *timeout;

@end

@implementation ACloudLib

- (id)init
{
    self = [super init];
    if (self) {
        self.host = BASE_URL_STRING;
        self.majorDomain = MAJOR_DOMAIN;
        self.majorDomainId = MAJOR_DOMAIN_ID;
        self.timeout = TIMEOUT;
    }
    return self;
}

+ (instancetype)sharedLib
{
    static id instance;
    static dispatch_once_t predicate;
    
    dispatch_once(&predicate, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}

+ (void)setHost:(NSString *)host
{
    [ACloudLib sharedLib].host = host;
}

+ (NSString *)getHost
{
    return [ACloudLib sharedLib].host;
}

+ (void)setMajorDomain:(NSString *)majorDomain majorDomainId:(NSInteger)majorDomainId
{
    [ACloudLib sharedLib].majorDomain = majorDomain;
    [ACloudLib sharedLib].majorDomainId = majorDomainId;
}

+ (NSString *)getMajorDomain
{
    return [ACloudLib sharedLib].majorDomain;
}

+ (NSInteger)getMajorDomainId
{
    return [ACloudLib sharedLib].majorDomainId;
}

+ (void)setHttpRequestTimeout:(NSString *)timeout
{
    [ACloudLib sharedLib].timeout = timeout;
}

+ (NSString *)getHttpRequestTimeout
{
    return [ACloudLib sharedLib].timeout;
}


+ (void)sendToService:(NSString *)subDomain
          serviceName:(NSString *)name
              version:(NSInteger)version
                  msg:(ACMsg *)msg
             callback:(void (^)(ACMsg *responseMsg, NSError *error))callback
{
    ACServiceClient *client = [ACServiceClient serviceClientWithHost:[ACloudLib getHost]
                                                             service:name
                                                             version:version];
    msg.context = [ACContext generateContextWithSubDomain:subDomain];
    [client sendToService:msg
                 callback:^(ACMsg *responseObject, NSError *error) {
                     callback(responseObject, error);
                 }];
}

@end
