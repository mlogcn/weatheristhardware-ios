//
//  ACServiceClient.m
//  ACloudLib
//
//  Created by zhourx5211 on 12/11/14.
//  Copyright (c) 2014 zcloud. All rights reserved.
//

#import "ACServiceClient.h"
#import "ACServiceStub.h"
//#import "ACDeviceMsg.h"

typedef enum : NSUInteger {
    ACServiceClientWrongPayloadFormat = 1000,
    ACServiceClientNoMsgName,
    ACServiceClientInvalidPayload,
}ACServiceClientError;

@implementation ACServiceClient
{
    NSOperationQueue *queue;
}

- (id)initWithHost:(NSString *)host service:(NSString *)service version:(NSInteger)version
{
    self = [super init];
    if (self) {
        self.host = host;
        self.service = service;
        self.serviceVersion = version;
    }
    return self;
}

+ (instancetype)serviceClientWithHost:(NSString *)host service:(NSString *)service version:(NSInteger)version
{
    return [[ACServiceClient alloc] initWithHost:host service:service version:version];
}

- (void)sendToService:(ACMsg *)req callback:(void (^)(ACMsg *responseObject, NSError *error))callback
{
    if ([ACServiceStub isServiceStub:self.service]) {
        id delegate = [ACServiceStub getServiceStubDelegate:self.service];
        if ([delegate respondsToSelector:@selector(handleServiceMsg:callback:)]) {
            [delegate handleServiceMsg:req callback:callback];
            return;
        }
    }
    
    NSError *reqError = nil;
    NSURLRequest *request = [self getRequest:req error:&reqError];
    if (reqError) {
        callback(nil, reqError);
        return;
    }
    queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:queue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               dispatch_async(dispatch_get_main_queue(), ^{
                                   if (connectionError) {
                                       callback(nil, connectionError);
                                       return;
                                   } else {
                                       NSError *e = nil;
                                       ACMsg *msg = [self parseResp:response data:data error:&e];
                                       callback(msg, e);
                                       return;
                                   }
                                   
                               });
                           }];
}

- (NSURLRequest *)getRequest:(ACMsg *)req error:(NSError **)error
{
    NSMutableString *fullUrl = [NSMutableString stringWithFormat:@"%@/%@/v%ld/%@", self.host, self.service, (long)self.serviceVersion, req.name];
    NSString *payloadFormat = req.payloadFormat;
    NSInteger length = req.payloadSize;
    NSData *payload = req.payload;
    if (payloadFormat == nil && [req hasObjectData]) {
        payload = [req marshal];
        payloadFormat = ACMsgObjectPayload;
        length = payload.length;
        [req setPayload:payload format:payloadFormat];
    }
    if (payloadFormat == nil ||
        (![payloadFormat isEqualToString:ACMsgStreamPayload] &&
        ![payloadFormat isEqualToString:ACMsgObjectPayload] &&
        ![payloadFormat isEqualToString:ACMsgJsonPayload])) {
            *error = [NSError errorWithDomain:@"wrong request payload format"
                                        code:ACServiceClientWrongPayloadFormat
                                    userInfo:nil];
        }
    if ([payloadFormat isEqualToString:ACMsgStreamPayload]) {
        NSArray *keys = [req getKeys];
        if (keys.count > 0) {
            [fullUrl appendString:@"?"];
            for (NSString *key in keys) {
                id value = [req get:key];
                if ([value isKindOfClass:[NSString class]]
                    || [value isKindOfClass:[NSNumber class]]) {
                    [fullUrl appendFormat:@"%@=%@&", key, value];
                }
            }
            fullUrl = [[fullUrl substringToIndex:(fullUrl.length - 1)] mutableCopy];
        }
        NSData *streamPayload = req.streamPayload;
        payload = streamPayload;
    }
    
    NSMutableURLRequest *reqest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:fullUrl]];
    [reqest setCachePolicy:NSURLRequestUseProtocolCachePolicy];
    [reqest setHTTPBody:payload];
    [reqest setHTTPMethod:@"POST"];
    NSMutableDictionary *headers = [[NSMutableDictionary alloc] init];
    [headers setObject:payloadFormat forKey:@"Content-Type"];
    [headers setObject:@(length).stringValue forKey:@"Content-Length"];
    [headers setObject:@(length).stringValue forKey:@"X-Zc-Content-Length"];
    if (req.context) {
        if (req.context.majorDomain) {
            [headers setObject:req.context.majorDomain forKey:@"X-Zc-Major-Domain"];
        }
        if (req.context.subDomain) {
            [headers setObject:req.context.subDomain forKey:@"X-Zc-Sub-Domain"];
        }
        if (req.context.userId) {
            [headers setObject:req.context.userId.stringValue forKey:@"X-Zc-User-Id"];
        }
        if (req.context.traceId) {
            [headers setObject:req.context.traceId forKey:@"X-Zc-Traceid"];
        }
        if (req.context.traceStartTime) {
            [headers setObject:req.context.traceStartTime forKey:@"X-Zc-Start-Time"];
        }
        if (req.context.timestamp) {
            [headers setObject:req.context.timestamp forKey:@"X-Zc-Timestamp"];
        }
        if (req.context.timeout) {
            [headers setObject:req.context.timeout forKey:@"X-Zc-Timeout"];
        }
        if (req.context.nonce) {
            [headers setObject:req.context.nonce forKey:@"X-Zc-Nonce"];
        }
        if (req.context.signature) {
            [headers setObject:req.context.signature forKey:@"X-Zc-User-Signature"];
        }
    }
    [reqest setAllHTTPHeaderFields:headers];
    return [reqest copy];
}

- (ACMsg *)parseResp:(NSURLResponse *)response data:(NSData *)data error:(NSError **)error
{
    ACMsg *resp = [[ACMsg alloc] init];
    NSDictionary *headers = [(NSHTTPURLResponse *)response allHeaderFields];
    NSString *msgName = headers[ACMsgMsgNameHeader];
    if (!msgName) {
        *error = [NSError errorWithDomain:self.service
                                     code:ACServiceClientNoMsgName
                                 userInfo:@{@"errorInfo": @"no msg name"}];
        return nil;
    }
    
    resp.name = msgName;
    NSData *content = data;
    if (content == nil) {
        return resp;
    }
    
    
    NSString *payloadFormat = headers[@"Content-Type"];
    if (payloadFormat == nil || content == nil) {
        *error = [NSError errorWithDomain:self.service
                                     code:ACServiceClientInvalidPayload
                                 userInfo:@{@"errorInfo": @"invalid payload"}];
        return nil;
    }
    if (![payloadFormat isEqualToString:ACMsgStreamPayload]
        && ![payloadFormat isEqualToString:ACMsgObjectPayload]
        && ![payloadFormat isEqualToString:ACMsgJsonPayload]
        && ![payloadFormat isEqualToString:@""]) {
        NSString *errorMsg = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        *error = [NSError errorWithDomain:self.service
                                     code:1
                                 userInfo:@{@"errorInfo": errorMsg}];
        return nil;
    }
    if ([payloadFormat isEqualToString:ACMsgStreamPayload]) {
        for (NSString *httpHead in headers.allKeys) {
            NSString *prefix = @"X-Zc-Msg-Attr_";
            if ([httpHead hasPrefix:prefix]) {
                NSString *key = [httpHead substringFromIndex:prefix.length];
                [resp put:key value:[headers objectForKey:httpHead]];
            }
        }
        [resp setStreamPayload:content size:content.length];
        return resp;
    }
    
    [resp setPayload:content format:payloadFormat];
    if ([payloadFormat isEqualToString:ACMsgObjectPayload]) {
        if ([resp isErr]) {
            resp = [ACMsg unmarshal:content];
            resp.name = ACMsgErrMSG;
            *error = [NSError errorWithDomain:self.service
                                         code:[resp getErrCode]
                                     userInfo:@{@"errorInfo": [resp getErrMsg]}];
            return nil;
        } else {
            resp = [ACMsg unmarshal:content];
            resp.name = ACMsgAckMSG;
            resp.payloadFormat = payloadFormat;
        }
    }
    return resp;
}

@end
