//
//  ACloudLib.h
//  ACloudLib
//
//  Created by zhourx5211 on 14/12/8.
//  Copyright (c) 2014年 zcloud. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ACMsg.h"
//#import "ACDeviceMsg.h"

@interface ACloudLib : NSObject

+ (void)setHost:(NSString *)host;
+ (NSString *)getHost;

+ (void)setMajorDomain:(NSString *)majorDomain majorDomainId:(NSInteger)majorDomainId;
+ (NSString *)getMajorDomain;
+ (NSInteger)getMajorDomainId;

+ (void)setHttpRequestTimeout:(NSString *)timeout;
+ (NSString *)getHttpRequestTimeout;

+ (void)sendToService:(NSString *)subDomain
          serviceName:(NSString *)name
              version:(NSInteger)version
                  msg:(ACMsg *)msg
             callback:(void (^)(ACMsg *responseMsg, NSError *error))callback;

@end
