//
//  ACContext.m
//  ACloudLib
//
//  Created by zhourx5211 on 12/11/14.
//  Copyright (c) 2014 zcloud. All rights reserved.
//

#import "ACContext.h"
#import "ACloudLib.h"
#import "ACKeyChain.h"
#import "ACHelper.h"

@implementation ACContext

+ (ACContext *)generateContextWithSubDomain:(NSString *)subDomain
{
    ACContext *context = [[ACContext alloc] init];
    context.os = @"ios";
    context.version = [UIDevice currentDevice].systemVersion;
    context.traceId = [UIDevice currentDevice].identifierForVendor.UUIDString;
    context.traceStartTime = [NSString stringWithFormat:@"%.f", [NSDate date].timeIntervalSince1970 - 10];
    context.majorDomain = [ACloudLib getMajorDomain];
    context.subDomain = subDomain;
    context.userId = [ACKeyChain getUserId];
    if (context.userId) {
        context.timestamp = context.traceStartTime;
        context.timeout = [ACloudLib getHttpRequestTimeout];
        context.nonce = [ACHelper generateNonceWithLength:15];
        context.signature = [ACHelper generateSignatureWithTimeout:context.timeout
                                                        timestamp:context.timestamp
                                                            nonce:context.nonce
                                                            token:[ACKeyChain getToken]];
    }
    return context;
}

@end
