//
//  ACAccountManager.m
//  ACloudLib
//
//  Created by zhourx5211 on 14/12/8.
//  Copyright (c) 2014年 zcloud. All rights reserved.
//

#import "ACAccountManager.h"
#import "ACloudLib.h"
#import "ACServiceClient.h"
#import "ACKeyChain.h"

#define USER_METHOD_SENDVERIFYCODE @"sendVerifyCode"
#define USER_METHOD_CHECKVERIFYCODE @"checkVerifyCode"
#define USER_METHOD_REGISTER @"register"
#define USER_METHOD_REGISTERWITHOPENID @"registerWithOpenId"

#define USER_METHOD_LOGIN @"login"
#define USER_METHOD_LOGINWITHOPENID @"loginWithOpenId"
#define USER_METHOD_RESETPASSWORD @"resetPassword"
#define USER_METHOD_CHANGEPASSWORD @"changePassword"

#define USER_METHOD_CHANGEPHONE @"changePhone"
#define USER_METHOD_CHECKEXIST @"checkExist"

@implementation ACAccountManager

+ (void)saveLoginData:(ACMsg *)responseObject error:(NSError **)error
{
    if (responseObject) {
        if ([responseObject isErr]) {
            NSString *errMsg = [responseObject getErrMsg];
            *error = [NSError errorWithDomain:@"account service error"
                                         code:[responseObject getErrCode]
                                     userInfo:@{@"errorInfo": errMsg}];
        }
        if ([responseObject contains:@"token"]) {
            [ACKeyChain saveToken:[responseObject get:@"token"]];
        }
        if ([responseObject contains:@"userId"]) {
            [ACKeyChain saveUserId:[responseObject get:@"userId"]];
        }
    }
}

// 获取验证码
+ (void)sendVerifyCodeWithAccount:(NSString *)account
                         template:(NSInteger)template
                         callback:(void (^)(NSError *error))callback
{
    ACServiceClient *client = [ACServiceClient serviceClientWithHost:[ACloudLib getHost]
                                                             service:USER_SERVICE
                                                             version:1];
    ACMsg *msg = [[ACMsg alloc] init];
    msg.context = [ACContext generateContextWithSubDomain:nil];
    msg.name = USER_METHOD_SENDVERIFYCODE;
    if (account) {
        [msg put:@"account" value:account];
    }
    
    [msg putInteger:@"template" value:template];
    
    [client sendToService:msg callback:^(ACMsg *responseObject, NSError *error) {
        callback(error);
    }];
}

+ (void)checkVerifyCodeWithAccount:(NSString *)account
                        verifyCode:(NSString *)verifyCode
                          callback:(void (^)(BOOL valid,NSError *error))callback
{
    ACServiceClient *client = [ACServiceClient serviceClientWithHost:[ACloudLib getHost]
                                                             service:USER_SERVICE
                                                             version:1];
    ACMsg *msg = [[ACMsg alloc] init];
    msg.context = [ACContext generateContextWithSubDomain:nil];
    msg.name = USER_METHOD_CHECKVERIFYCODE;
    if (account) {
        [msg put:@"account" value:account];
    }
    if (verifyCode) {
        [msg put:@"verifyCode" value:verifyCode];
    }
    [client sendToService:msg callback:^(ACMsg *responseObject, NSError *error) {
        if ([responseObject contains:@"valid"]) {
            BOOL online = [responseObject getBool:@"valid"];
            callback(online, error);
        } else {
            callback(NO, error);
        }
    }];
}

// 注册
+ (void)registerWithPhone:(NSString *)phone
                    email:(NSString *)email
                 password:(NSString *)password
               verifyCode:(NSString *)verifyCode
                 callback:(void (^)(NSString *uid, NSError *error))callback
{
    ACServiceClient *client = [ACServiceClient serviceClientWithHost:[ACloudLib getHost]
                                                             service:USER_SERVICE
                                                             version:1];
    ACMsg *msg = [[ACMsg alloc] init];
    msg.context = [ACContext generateContextWithSubDomain:nil];
    msg.name = USER_METHOD_REGISTER;
    if (phone) {
        [msg put:@"phone" value:phone];
    }
    if (email) {
        [msg put:@"email" value:email];
    }
    if (password) {
        [msg put:@"password" value:password];
    }
    if (verifyCode) {
        [msg put:@"verifyCode" value:verifyCode];
    }
    [client sendToService:msg callback:^(ACMsg *responseObject, NSError *error) {
        if (error) {
            callback(nil, error);
        } else {
            [self saveLoginData:responseObject error:&error];
            callback([responseObject get:@"userId"], nil);
        }
    }];
}

//三方注册
+ (void)registerWithOpenId:(NSString *)openId
                  openType:(NSString *)openType
               accessToken:(NSString *)accessToken
                  callback:(void (^)(NSString *uid, NSError *error))callback
{
    ACServiceClient *client = [ACServiceClient serviceClientWithHost:[ACloudLib getHost]
                                                             service:USER_SERVICE
                                                             version:1];
    ACMsg *msg = [[ACMsg alloc] init];
    msg.context = [ACContext generateContextWithSubDomain:nil];
    msg.name = USER_METHOD_REGISTERWITHOPENID;
    if (openId) {
        [msg put:@"openId" value:openId];
    }
    if (openType) {
        [msg put:@"openType" value:openType];
    }
    if (accessToken) {
        [msg put:@"accessToken" value:accessToken];
    }
    
    [client sendToService:msg callback:^(ACMsg *responseObject, NSError *error) {
        if (error) {
            callback(nil, error);
        } else {
            [self saveLoginData:responseObject error:&error];
            callback([responseObject get:@"userId"], nil);
        }
    }];

    
}


+ (void)loginWithAccount:(NSString *)account
                password:(NSString *)password
                callback:(void (^)(NSString *uid, NSError *error))callback
{
    ACServiceClient *client = [ACServiceClient serviceClientWithHost:[ACloudLib getHost]
                                                             service:USER_SERVICE
                                                             version:1];
    ACMsg *msg = [[ACMsg alloc] init];
    msg.context = [ACContext generateContextWithSubDomain:nil];
    msg.name = USER_METHOD_LOGIN;
    if (account) {
        [msg put:@"account" value:account];
    }
    if (password) {
        [msg put:@"password" value:password];
    }
    [client sendToService:msg callback:^(ACMsg *responseObject, NSError *error) {
        if (error) {
            callback(nil, error);
        } else {
            [self saveLoginData:responseObject error:&error];
            callback([responseObject get:@"userId"], nil);
        }
    }];
}

//三方登陆
+ (void)loginWithOpenId:(NSString *)openId
               openType:(NSString *)openType
            accessToken:(NSString *)accessToken
               callback:(void (^)(NSString *uid, NSError *error))callback
{
    ACServiceClient *client = [ACServiceClient serviceClientWithHost:[ACloudLib getHost]
                                                             service:USER_SERVICE
                                                             version:1];
    ACMsg *msg = [[ACMsg alloc] init];
    msg.context = [ACContext generateContextWithSubDomain:nil];
    msg.name = USER_METHOD_LOGINWITHOPENID;
    if (openId) {
        [msg put:@"openId" value:openId];
    }
    if (openType) {
        [msg put:@"openType" value:openType];
    }
    if (accessToken) {
        [msg put:@"accessToken" value:accessToken];
    }
    [client sendToService:msg callback:^(ACMsg *responseObject, NSError *error) {
        if (error) {
            callback(nil, error);
        } else {
            [self saveLoginData:responseObject error:&error];
            callback([responseObject get:@"userId"], nil);
        }
    }];
}


// 重置密码
+ (void)resetPasswordWithAccount:(NSString *)account
                      verifyCode:(NSString *)verifyCode
                        password:(NSString *)password
                        callback:(void (^)(NSString *uid, NSError *error))callback
{
    ACServiceClient *client = [ACServiceClient serviceClientWithHost:[ACloudLib getHost]
                                                             service:USER_SERVICE
                                                             version:1];
    ACMsg *msg = [[ACMsg alloc] init];
    msg.context = [ACContext generateContextWithSubDomain:nil];
    msg.name = USER_METHOD_RESETPASSWORD;
    if (account) {
        [msg put:@"account" value:account];
    }
    if (verifyCode) {
        [msg put:@"verifyCode" value:verifyCode];
    }
    if (password) {
        [msg put:@"password" value:password];
    }
    [client sendToService:msg callback:^(ACMsg *responseObject, NSError *error) {
        if (error) {
            callback(nil, error);
        } else {
            [self saveLoginData:responseObject error:&error];
            callback([responseObject get:@"userId"], nil);
        }
    }];
}

// 修改密码
+ (void)changePasswordWithOld:(NSString *)oldPassword
                          new:(NSString *)newPassword
                     callback:(void (^)(NSString *uid, NSError *error))callback
{
    ACServiceClient *client = [ACServiceClient serviceClientWithHost:[ACloudLib getHost]
                                                             service:USER_SERVICE
                                                             version:1];
    ACMsg *msg = [[ACMsg alloc] init];
    msg.context = [ACContext generateContextWithSubDomain:nil];
    msg.name = USER_METHOD_CHANGEPASSWORD;
    if (oldPassword) {
        [msg put:@"old" value:oldPassword];
    }
    if (newPassword) {
        [msg put:@"new" value:newPassword];
    }
    [client sendToService:msg callback:^(ACMsg *responseObject, NSError *error) {
        if (error) {
            callback(nil, error);
        } else {
            [self saveLoginData:responseObject error:&error];
            callback([responseObject get:@"userId"], nil);
        }
    }];
}

+ (void)changePhone:(NSString *)phone
           password:(NSString *)password
         verifyCode:(NSString *)verifyCode
           callback:(void (^)(NSError *))callback
{
    ACServiceClient *client = [ACServiceClient serviceClientWithHost:[ACloudLib getHost]
                                                             service:USER_SERVICE
                                                             version:1];
    ACMsg *msg = [[ACMsg alloc]init];
    msg.context = [ACContext generateContextWithSubDomain:nil];
    msg.name = USER_METHOD_CHANGEPHONE;
    if (phone) {
        [msg put:@"phone" value:phone];
    }
    if (password) {
        [msg put:@"password" value:password];
    }
    if (verifyCode) {
        [msg put:@"verifyCode" value:verifyCode];
    }
    [client sendToService:msg callback:^(ACMsg *responseObject, NSError *error) {
        if (error) {
            callback(error);
        }else
        {
            callback(nil);
        }
    }];
}

+(void)checkExist:(NSString *)account
         callback:(void (^)(BOOL exist, NSError * error))callback
{
    
    ACServiceClient *client = [ACServiceClient serviceClientWithHost:[ACloudLib getHost]
                                                             service:USER_SERVICE
                                                             version:1
                               ];
    ACMsg *msg = [[ACMsg alloc]init];
    msg.context = [ACContext generateContextWithSubDomain:nil];
    msg.name = USER_METHOD_CHECKEXIST;
    if (account) {
        [msg put:@"account" value:account];
    }
    [client sendToService:msg callback:^(ACMsg *responseObject, NSError *error) {
        if (error) {
            callback(NO,error);
        }else{
            callback([responseObject getBool:@"exist"],nil);
        }
    }];
}

+ (BOOL)isLogin
{
    NSNumber *userId = [[ACKeyChain getUserId] copy];
    
    return (userId == nil) ? NO: YES;
}
+ (void)logout
{
    [ACKeyChain removeUserId];
    [ACKeyChain removeToken];
}
@end
