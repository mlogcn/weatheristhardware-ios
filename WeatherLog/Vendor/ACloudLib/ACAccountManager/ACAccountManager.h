//
//  ACAccountManager.h
//  ACloudLib
//
//  Created by zhourx5211 on 14/12/8.
//  Copyright (c) 2014年 zcloud. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ACMsg.h"

#define USER_SERVICE @"zc-account"

@interface ACAccountManager : NSObject

/**
 *  获取验证码
 */ 
+ (void)sendVerifyCodeWithAccount:(NSString *)account
                         template:(NSInteger)template
                         callback:(void (^)(NSError *error))callback;

/**
 *  验证验证码
 */
+ (void)checkVerifyCodeWithAccount:(NSString *)account
                        verifyCode:(NSString *)verifyCode
                          callback:(void (^)(BOOL valid,NSError *error))callback;

/**
 *  注册
 */
+ (void)registerWithPhone:(NSString *)phone
                    email:(NSString *)email
                 password:(NSString *)password
               verifyCode:(NSString *)verifyCode
                 callback:(void (^)(NSString *uid, NSError *error))callback;


/**
 *  三方注册
 */
+ (void)registerWithOpenId:(NSString *)openId
                    openType:(NSString *)openType
                 accessToken:(NSString *)accessToken
                 callback:(void (^)(NSString *uid, NSError *error))callback;
/**
 *  登录
 */
+ (void)loginWithAccount:(NSString *)account
                password:(NSString *)password
                callback:(void (^)(NSString *uid, NSError *error))callback;
/**
 *  三方登陆
 */
+ (void)loginWithOpenId:(NSString *)openId
                  openType:(NSString *)openType
               accessToken:(NSString *)accessToken
                  callback:(void (^)(NSString *uid, NSError *error))callback;
/**
 *  重置密码
 */
+ (void)resetPasswordWithAccount:(NSString *)account
                      verifyCode:(NSString *)verifyCode
                        password:(NSString *)password
                        callback:(void (^)(NSString *uid, NSError *error))callback;
/**
 *  修改密码
 */
+ (void)changePasswordWithOld:(NSString *)old
                          new:(NSString *)newPassword
                     callback:(void (^)(NSString *uid, NSError *error))callback;

/**
 *  更换手机号
 */
+ (void)changePhone:(NSString *)phone
           password:(NSString *)password
         verifyCode:(NSString *)verifyCode
           callback:(void(^)(NSError *error)) callback;
/**
 *  判断用户是否已经存在
 */
+ (void)checkExist:(NSString *)account
          callback:(void(^)(BOOL exist,NSError *error))callback;

/**
 *  判断用户是否已经在本机上过登陆
 */
+ (BOOL)isLogin;

/**
 *  注销当前用户
 */
+ (void)logout;
@end
