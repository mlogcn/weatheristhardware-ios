//
//  ACObject.m
//  ACloudLib
//
//  Created by zhourx5211 on 12/10/14.
//  Copyright (c) 2014 zcloud. All rights reserved.
//

#import "ACObject.h"

typedef enum : NSUInteger {
    ACObjectErrorUnsupportedType = 1000,
}ACObjectError;

@interface ACObject ()

@property (strong, nonatomic) NSMutableDictionary *data;

@end

@implementation ACObject

- (id)init
{
    self = [super init];
    if (self) {
        self.data = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (BOOL)checkType:(id)object
{
    return object == nil ||
    [object isKindOfClass:[NSString class]] ||
    [object isKindOfClass:[NSNumber class]] ||
    [object isKindOfClass:[NSArray class]] ||
    [object isKindOfClass:[ACObject class]];
}

- (void)ensureType:(id)object error:(NSError **)error
{
    if (![self checkType:object]) {
        *error = [NSError errorWithDomain:@"unsupported type"
                                     code:ACObjectErrorUnsupportedType
                                 userInfo:nil];
    }
}

- (id)get:(NSString *)name
{
    return [self.data objectForKey:name];
}

- (NSArray *)getArray:(NSString *)name
{
    id value = [self.data objectForKey:name];
    if ([value isKindOfClass:[NSArray class]]) {
        return value;
    } else {
        return nil;
    }
}

- (BOOL)getBool:(NSString *)name
{
    NSNumber *number = [self.data objectForKey:name];
    if ([number isKindOfClass:[NSNumber class]]) {
        return number.boolValue;
    }
    return NO;
}

- (long)getLong:(NSString *)name
{
    NSNumber *number = [self.data objectForKey:name];
    if ([number isKindOfClass:[NSNumber class]]) {
        return number.longValue;
    }
    return 0;
}

- (NSInteger)getInteger:(NSString *)name
{
    NSNumber *number = [self.data objectForKey:name];
    if ([number isKindOfClass:[NSNumber class]]) {
        return number.integerValue;
    }
    return 0;
}

- (float)getFloat:(NSString *)name
{
    NSNumber *number = [self.data objectForKey:name];
    if ([number isKindOfClass:[NSNumber class]]) {
        return number.floatValue;
    }
    return 0.f;
}

- (double)getDouble:(NSString *)name
{
    NSNumber *number = [self.data objectForKey:name];
    if ([number isKindOfClass:[NSNumber class]]) {
        return number.doubleValue;
    }
    return 0.f;
}

- (NSString *)getString:(NSString *)name
{
    NSString *string = [self.data objectForKey:name];
    if ([string isKindOfClass:[NSString class]]) {
        return string;
    }
    return nil;
}

- (ACObject *)getACObject:(NSString *)name
{
    ACObject *object = [self.data objectForKey:name];
    if ([object isKindOfClass:[ACObject class]]) {
        return object;
    }
    return nil;
}


- (void)put:(NSString *)name value:(id)value
{
    NSError *error = nil;
    [self ensureType:value error:&error];
    if (error) {
        NSLog(@"%@", error);
    } else {
        [self.data setObject:value forKey:name];
    }
}

- (void)putArray:(NSArray *)name value:(id)value
{
    NSError *error = nil;
    [self ensureType:value error:&error];
    if (error) {
        NSLog(@"%@", error);
    } else {
        [self.data setObject:value forKey:name];
    }
}

- (void)putBool:(NSString *)name value:(BOOL)value
{
    NSNumber *number = [NSNumber numberWithBool:value];
    [self.data setObject:number forKey:name];
}

- (void)putLong:(NSString *)name value:(long)value
{
    NSNumber *number = [NSNumber numberWithLong:value];
    [self.data setObject:number forKey:name];
}

- (void)putInteger:(NSString *)name value:(NSInteger)value
{
    NSNumber *number = [NSNumber numberWithInteger:value];
    [self.data setObject:number forKey:name];
}

- (void)putFloat:(NSString *)name value:(float)value
{
    NSNumber *number = [NSNumber numberWithFloat:value];
    [self.data setObject:number forKey:name];
}

- (void)putDouble:(NSString *)name value:(double)value
{
    NSNumber *number = [NSNumber numberWithDouble:value];
    [self.data setObject:number forKey:name];
}

- (void)putString:(NSString *)name value:(NSString *)value
{
    [self.data setObject:value forKey:name];
}

- (void)putACObject:(NSString *)name value:(ACObject *)value
{
    [self.data setObject:value forKey:name];
}


- (void)add:(NSString *)name value:(id)value
{
    NSError *error = nil;
    [self ensureType:value error:&error];
    if (error) {
        NSLog(@"%@", error);
    } else {
        id object = [self.data objectForKey:name];
        id newObject = nil;
        if (!object) {
            newObject = @[value];
        } else if ([object isKindOfClass:[NSArray class]]) {
            NSMutableArray *array = [object mutableCopy];
            [array addObject:value];
            newObject = [array copy];
        }
        [self.data setObject:newObject forKey:name];
    }
}

- (void)addBool:(NSString *)name value:(BOOL)value
{
    NSNumber *number = [NSNumber numberWithBool:value];
    [self add:name value:number];
}

- (void)addLong:(NSString *)name value:(long)value
{
    NSNumber *number = [NSNumber numberWithLong:value];
    [self add:name value:number];
}

- (void)addInteger:(NSString *)name value:(NSInteger)value
{
    NSNumber *number = [NSNumber numberWithInteger:value];
    [self add:name value:number];
}

- (void)addFloat:(NSString *)name value:(float)value
{
    NSNumber *number = [NSNumber numberWithFloat:value];
    [self add:name value:number];
}

- (void)addDouble:(NSString *)name value:(double)value
{
    NSNumber *number = [NSNumber numberWithDouble:value];
    [self add:name value:number];
}

- (void)addString:(NSString *)name value:(NSString *)value
{
    [self add:name value:value];
}

- (void)addACObject:(NSString *)name value:(ACObject *)value
{
    [self add:name value:value];
}


- (BOOL)contains:(NSString *)name
{
    return [[self.data allKeys] containsObject:name];
}

- (NSArray *)getKeys
{
    return [self.data allKeys];
}


- (BOOL)hasObjectData
{
    return self.data != nil;
}

- (NSDictionary *)getObjectData
{
    return [self.data copy];
}

- (void)setObjectData:(NSDictionary *)data
{
    self.data = [data mutableCopy];
}

- (NSDictionary *)parseToDictionary
{
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
    for (NSString *key in [self getKeys]) {
        id value = [self get:key];
        if ([value isKindOfClass:[NSString class]] ||
            [value isKindOfClass:[NSNumber class]] ||
            [value isKindOfClass:[NSNull class]]) {
            [dictionary setObject:value forKey:key];
        }
        else if ([value isKindOfClass:[NSArray class]]){
            NSArray *tempArr = (NSArray *)value;
            if ([tempArr.firstObject isMemberOfClass:[ACObject class]]) {
                NSMutableArray *arr = [NSMutableArray array];
                for (ACObject *obj in tempArr) {
                    [arr addObject:[obj parseToDictionary]];
                }
                [dictionary setObject:arr forKey:key];
            }
            else{
                [dictionary setObject:value forKey:key];
            }
        }
        else if ([value isMemberOfClass:[ACObject class]]) {
            [dictionary setObject:[(ACObject *)value parseToDictionary] forKey:key];
        }
    }
    return dictionary;
}

- (NSData *)marshal
{
    NSDictionary *dictionary = [self parseToDictionary];
    NSError *error = nil;
    NSData *data = [NSJSONSerialization dataWithJSONObject:dictionary
                                                   options:NSJSONWritingPrettyPrinted
                                                     error:&error];
    if (dictionary.allKeys == 0) {
        data = nil;
    }
    if (error) {
        NSLog(@"%@", error);
        return nil;
    }
    return data;
}

+ (NSData *)marshal:(ACObject *)object
{
    return [object marshal];
}

+ (instancetype)parseFromDictionary:(NSDictionary *)dictionary
{
    id object = [[self alloc] init];
    if (object && dictionary) {
        NSMutableDictionary *data = [dictionary mutableCopy];
        for (NSString *key in data.allKeys) {
            id value = dictionary[key];
            if ([value isKindOfClass:[NSDictionary class]]) {
                id object = [self parseFromDictionary:value];
                [data setObject:object forKey:key];
            }
        }
        [(ACObject *)object setData:data];
    }
    return object;
}

+ (instancetype)unmarshal:(NSData *)data
{
    NSDictionary *dictionary = [[NSMutableDictionary alloc] init];
    if (data && data.length > 0) {
        NSError *error = nil;
        id json = [NSJSONSerialization JSONObjectWithData:data
                                                  options:NSJSONReadingMutableContainers
                                                    error:&error];
        if ([json isKindOfClass:[NSDictionary class]]) {
            dictionary = json;
        }
        if ([json isKindOfClass:[NSArray class]]) {
            [dictionary setValue:json forKey:@"default"];
        }
        
        if (error) {
            NSLog(@"%@", error);
            return nil;
        }
    }
    return [self parseFromDictionary:dictionary];
}

@end
