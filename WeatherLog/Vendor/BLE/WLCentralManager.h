//
//  WLCentralManager.h
//  WeatherLog
//
//  Created by 龙恒舟 on 15/8/3.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import "YMSCBCentralManager.h"

@interface WLCentralManager : YMSCBCentralManager


/**
 生成单例
 */
+ (WLCentralManager *)initSharedServiceWithDelegate:(id)delegate;

/**
 获得单例
 */

+ (WLCentralManager *)sharedService;

@end
