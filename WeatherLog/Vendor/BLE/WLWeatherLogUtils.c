//
//  WLWeatherLogUtils.c
//  WeatherLog
//
//  Created by 龙恒舟 on 15/9/15.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#include "WLWeatherLogUtils.h"

double calcHumTmp(uint16_t rawT) {
    double v;
    v = -46.85 + 175.72/65536 *(double)rawT;
    return v;
}

double calcHumRel(uint16_t rawH) {
    double v;
    v = -6.0 + (125.0/65536.0) * (double)rawH;
    return v;
}

double calcBarPress(uint16_t t_r,
                     uint16_t p_r,
                     int16_t c1,
                     int16_t c2,
                     int16_t c3,
                     uint16_t c4,
                     uint16_t c5,
                     uint16_t c6,
                     int16_t c7,
                     int16_t c8,
                     int16_t c9,
                     int16_t c10,
                     int16_t c11,
                     double *temp) {
    
    double X1, X2, B5, t_a, p_a;
    
    X1 = (t_r - (int32_t)c6 ) * (int32_t)c5 / (double)(0x8000);
    X2 = ((int32_t)c10 * 0x800) / (double)(X1 + (int32_t)c11);
    B5 = X1 + X2;
    t_a = (B5 + 8) / (double)0x10;
    *temp = (double)t_a / 10.0;
    
    double B6, X3, B3;
    uint32_t B4, B7;
    B6 = B5 - 4000;
    X1 = ((int32_t)c8 * (B6 * B6 / (double)0x1000)) / (double)0x800;
    X2 = (int32_t)c2 * B6 / (double)0x800;
    X3 = X1 + X2;
    B3 = (((int32_t)c1 * 4 + X3)+2)/(double)4;
    X1 = (int32_t)c3 * B6 / (double)0x2000;
    X2 = ((int32_t)c7* (B6 * B6 / (double)0x1000)) / (double)0x10000;
    X3 = ((X1 + X2) + 2)/(double)4;
    B4 = (int32_t)c4 * ((uint32_t)(X3 + 32768)/(double)0x8000);
    B7 = ((uint32_t)p_r - B3) * 50000;
    if (B7 < 0x80000000) {
        p_a = (B7 * 2 / (double)B4);
    } else {
        p_a = (B7 / (double)B4) * 2;
    }
    X1 = (p_a / (double)0x100)*(p_a / (double)0x100);
    X1 = (X1 * 3038) / (double)0x10000;
    X2 = (-7357 * p_a) / (double)0x10000;
    p_a = p_a + (X1 + X2 + 3791) / (double)16;
    
    // Unit: Pascal (Pa)
    return (double)p_a;
}