//
//  WLWeatherLog.h
//  WeatherLog
//
//  Created by 龙恒舟 on 15/8/3.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import "YMSCBPeripheral.h"

@class WLHumidityService;
@class WLDeviceInfoService;
@class WLBarometerService;
@class WLUVIndexService;
@class WLBatteryService;
@class WLHistoryDataService;
@class WLMonitorService;
@class WLOADService;

@interface WLWeatherLog : YMSCBPeripheral

//@property (nonatomic, readonly) WLHumidityService *humidity;
@property (nonatomic, readonly) WLDeviceInfoService *devinfo;
//@property (nonatomic, readonly) WLBarometerService *barometer;
//@property (nonatomic, readonly) WLUVIndexService *uvindex;
@property (nonatomic, readonly) WLBatteryService *battery;
@property (nonatomic, readonly) WLHistoryDataService *historydata;
@property (nonatomic, readonly) WLMonitorService *monitorService;
@property (nonatomic, readonly) WLOADService *oadService;

@end
