//
//  WLWeatherLogUtils.h
//  WeatherLog
//
//  Created by 龙恒舟 on 15/9/23.
//  Copyright © 2015年 龙恒舟. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WLWeatherLogUtils : NSObject

@end

double calcHumTmp(uint16_t rawT);

double calcHumRel(uint16_t rawH);

double calcSenTemp(double temp, double humidity, double speed);

double calcBarPress(uint16_t t_r,
                    uint16_t p_r,
                    int16_t c1,
                    int16_t c2,
                    int16_t c3,
                    uint16_t c4,
                    uint16_t c5,
                    uint16_t c6,
                    int16_t c7,
                    int16_t c8,
                    int16_t c9,
                    int16_t c10,
                    int16_t c11,
                    double *temp);

bool calcAllMonitorValues(NSData *data, double *temp, double *humidity, double *pressure, double *uvi);
