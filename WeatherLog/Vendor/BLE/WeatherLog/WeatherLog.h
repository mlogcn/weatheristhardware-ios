//
//  WeatherLog.h
//  WeatherLog
//
//  Created by 龙恒舟 on 15/8/3.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#include "YMS128.h"


#ifndef WeatherLog_WeatherLog_h
#define WeatherLog_WeatherLog_h

//#define kWeatherLog_IDENTIFIER "9C4EEB7D-BE3A-E942-1539-CB7AD105CE5D"

#define kWeatherLog_BASE_ADDRESS_HI 0xF000000004514000
#define kWeatherLog_BASE_ADDRESS_LO 0xB000000000000000


#define kWeatherLog_GAP_SERVICE_UUID        0x1800
#define kWeatherLog_GATT_SERVICE_UUID       0x1801


#define kWeatherLog_DEVINFO_SERV_UUID       0x180A

#define kWeatherLog_DEVINFO_SYSTEM_ID       0x2A23
#define kWeatherLog_DEVINFO_MODEL_NUMBER    0x2A24
#define kWeatherLog_DEVINFO_SERIAL_NUMBER   0x2A25
#define kWeatherLog_DEVINFO_FIRMWARE_REV    0x2A26
#define kWeatherLog_DEVINFO_HARDWARE_REV    0x2A27
#define kWeatherLog_DEVINFO_SOFTWARE_REV    0x2A28
#define kWeatherLog_DEVINFO_MANUFACTURER_NAME 0x2A29
#define kWeatherLog_DEVINFO_11073_CERT_DATA 0x2A2A
#define kWeatherLog_DEVINFO_PNPID_DATA      0x2A2A

#pragma mlogcn defined

#define kWeatherLog_HUMIDITY_SERVICE        0xAA20
#define kWeatherLog_HUMIDITY_DATA           0xAA21
#define kWeatherLog_HUMIDITY_CONFIG         0xAA22

#define kWeatherLog_BAROMETER_SERVICE       0xAA40
#define kWeatherLog_BAROMETER_DATA          0xAA41
#define kWeatherLog_BAROMETER_CONFIG        0xAA42
#define kWeatherLog_BAROMETER_CALIBRATION   0xAA43
#define kWeatherLog_BAROMETER_CALIBRATION1  0xAA44


#define kWeatherLog_UVINDEX_SERVICE       0xAA50
#define kWeatherLog_UVINDEX_DATA          0xAA51
#define kWeatherLog_UVINDEX_CONFIG        0xAA52

#define kWeatherLog_BATTERY_SERVICE     0x180F
#define kWeatherLog_BATTERY_LEVEL       0X2A19

#define kWeatherLog_HISTORYDATA_SERVICE     0xAA70
#define kWeatherLog_HISTORYDATA_SET         0XAA71
#define kWeatherLog_HISTORYDATA_CONFIG      0xAA72
#define kWeatherLog_HISTORYDATA_COUNT       0xAA73
#define kWeatherLog_HISTORYDATA_RPTR        0xAA74
#define kWeatherLog_HISTORYDATA_RECORD      0xAA75

#define kWeatherLog_OAD_SERVICE_UUID        0xFFC0
#define kWeatherLog_OAD_IMAGE_NOTIFY_UUID   0xFFC1
#define kWeatherLog_OAD_IMAGE_BLOCK_REQUEST_UUID    0xFFC2
#endif
