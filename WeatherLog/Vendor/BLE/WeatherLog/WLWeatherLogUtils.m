//
//  WLWeatherLogUtils.m
//  WeatherLog
//
//  Created by 龙恒舟 on 15/9/23.
//  Copyright © 2015年 龙恒舟. All rights reserved.
//

#import "WLWeatherLogUtils.h"
#import "math.h"

@implementation WLWeatherLogUtils

@end

double calcSenTemp(double temp, double humidity, double speed) {
    double result = 0.0;
    
    double tu = 0.0;        //湿度修正
    if (humidity > 60.0) {
        tu = 0.00025 * (temp - 20.0) * pow(fabs(temp - 20.0), 0.2) * pow((humidity - 60.0), 2.0);
    }else if (humidity < 40.0) {
        tu = 0.00025 * (20.0 - temp) * pow(fabs(temp - 20.0), 0.2) * pow((humidity - 40.0), 2.0);
    }else {
        tu = 0.0;
    }
    
    double tv = 0.0;        //风寒修正
    double tf = temp * 1.8 + 32.0;
    double vm = speed * 3.6 / 1.609;
    if (vm <= 3.0) {
        tv = 0.0;
    }else {
        if (tf <= 50.0) {
            tv = (35.74 + 0.6215 * tf - 35.75 * pow(vm, 0.16) + 0.4275 * tf * pow(vm, 0.16) - tf) / 1.8;
        }else if (tf > 50.0 && tf <= 68.0) {
            tv = (20.0 - temp) / (30.0 - temp) * (35.74 + 0.6215 * (tf - 18.0) - 35.75 * pow(vm, 0.16) + 0.4275 * (tf - 18.0) * pow(vm, 0.16) - tf + 18.0) / 1.8;
        }else {
            tv = 0.0;
        }
    }
    result = temp + tu + tv;
    return result;
}

double calcHumTmp(uint16_t rawT) {
    double v;
    v = -46.85 + 175.72/65536 *(double)rawT;
    return v;
}

double calcHumRel(uint16_t rawH) {
    double v;
    v = -6.0 + (125.0/65536.0) * (double)rawH;
    return v;
}

double calcBarPress(uint16_t t_r,
                    uint16_t p_r,
                    int16_t c1,
                    int16_t c2,
                    int16_t c3,
                    uint16_t c4,
                    uint16_t c5,
                    uint16_t c6,
                    int16_t c7,
                    int16_t c8,
                    int16_t c9,
                    int16_t c10,
                    int16_t c11,
                    double *temp) {
    
    double X1, X2, B5, t_a, p_a;
    
    X1 = (t_r - (int32_t)c6 ) * (int32_t)c5 / (double)(0x8000);
    X2 = ((int32_t)c10 * 0x800) / (double)(X1 + (int32_t)c11);
    B5 = X1 + X2;
    t_a = (B5 + 8) / (double)0x10;
    *temp = (double)t_a / 10.0;
    
    double B6, X3, B3;
    uint32_t B4, B7;
    B6 = B5 - 4000;
    X1 = ((int32_t)c8 * (B6 * B6 / (double)0x1000)) / (double)0x800;
    X2 = (int32_t)c2 * B6 / (double)0x800;
    X3 = X1 + X2;
    B3 = (((int32_t)c1 * 4 + X3)+2)/(double)4;
    X1 = (int32_t)c3 * B6 / (double)0x2000;
    X2 = ((int32_t)c7* (B6 * B6 / (double)0x1000)) / (double)0x10000;
    X3 = ((X1 + X2) + 2)/(double)4;
    B4 = (int32_t)c4 * ((uint32_t)(X3 + 32768)/(double)0x8000);
    B7 = ((uint32_t)p_r - B3) * 50000;
    if (B7 < 0x80000000) {
        p_a = (B7 * 2 / (double)B4);
    } else {
        p_a = (B7 / (double)B4) * 2;
    }
    X1 = (p_a / (double)0x100)*(p_a / (double)0x100);
    X1 = (X1 * 3038) / (double)0x10000;
    X2 = (-7357 * p_a) / (double)0x10000;
    p_a = p_a + (X1 + X2 + 3791) / (double)16;
    
    // Unit: Pascal (Pa)
    return (double)p_a;
}
