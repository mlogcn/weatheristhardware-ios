//
//  WLWeatherLog.m
//  WeatherLog
//
//  Created by 龙恒舟 on 15/8/3.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import "WLWeatherLog.h"
#import "WLBaseService.h"
#import "WLDeviceInfoService.h"
#import "WLBatteryService.h"
#import "WLOADService.h"
#import "WLHistoryDataService.h"
#import "WLMonitorService.h"

#import "YMSCBCharacteristic.h"
#import "YMSCBDescriptor.h"
#import "WeatherLog.h"


@implementation WLWeatherLog

//返回值用instancetype，不用id
- (instancetype)initWithPeripheral:(CBPeripheral *)peripheral
                           central:(YMSCBCentralManager *)owner
                            baseHi:(int64_t)hi
                            baseLo:(int64_t)lo {
    
    self = [super initWithPeripheral:peripheral central:owner baseHi:hi baseLo:lo];
    
    if (self) {
//        WLUVIndexService *us = [[WLUVIndexService alloc] initWithName:@"uvindex" parent:self baseHi:hi baseLo:lo serviceOffset:kWeatherLog_UVINDEX_SERVICE];
//        
//        WLHumidityService *hs = [[WLHumidityService alloc] initWithName:@"humidity" parent:self baseHi:hi baseLo:lo serviceOffset:kWeatherLog_HUMIDITY_SERVICE];
//        
//        WLBarometerService *bs = [[WLBarometerService alloc] initWithName:@"barometer" parent:self baseHi:hi baseLo:lo serviceOffset:kWeatherLog_BAROMETER_SERVICE];
        
        WLDeviceInfoService *ds = [[WLDeviceInfoService alloc] initWithName:@"devinfo" parent:self baseHi:0 baseLo:0 serviceOffset:kWeatherLog_DEVINFO_SERV_UUID];
        
        WLBatteryService *bls = [[WLBatteryService alloc] initWithName:@"battery" parent:self baseHi:0 baseLo:0 serviceOffset:kWeatherLog_BATTERY_SERVICE];
        
        WLHistoryDataService *hd = [[WLHistoryDataService alloc] initWithName:@"historydata" parent:self baseHi:hi baseLo:lo serviceOffset:kWeatherLog_HISTORYDATA_SERVICE];
        
        WLMonitorService * ms = [[WLMonitorService alloc] initWithName:@"monitor" parent:self baseHi:hi baseLo:lo serviceOffset:kWeatherLog_BAROMETER_SERVICE];
        
        WLOADService * oads = [[WLOADService alloc] initWithName:@"oad" parent:self baseHi:hi baseLo:lo serviceOffset:kWeatherLog_OAD_SERVICE_UUID];
        
//        self.serviceDict = @{@"uvindex": us,
//                             @"humidity": hs,
//                             @"barometer": bs,
//                             @"devinfo": ds,
//                             @"battery": bls,
//                             @"historydata": hd};
        self.serviceDict = @{@"monitor": ms,
                             @"devinfo": ds,
                             @"battery": bls,
                             @"historydata": hd,
                             @"oad": oads};

    }
    return self;
}


- (void)connect {
    // Watchdog aware method
    [self resetWatchdog];
    
    [self connectWithOptions:nil withBlock:^(YMSCBPeripheral *yp, NSError *error) {
        if (error) {
            return;
        }
        
        // Example where only a subset of services is to be discovered.
        //[yp discoverServices:[yp servicesSubset:@[@"temperature", @"simplekeys", @"devinfo"]] withBlock:^(NSArray *yservices, NSError *error) {
        
        NSLog(@"The services contains: %@.", [yp services]);
        [yp discoverServices:[yp services] withBlock:^(NSArray *yservices, NSError *error) {
            if (error) {
                return;
            }
            NSLog(@"The services contains: %@.", yservices);
            for (YMSCBService *service in yservices) {
                NSLog(@"The service %@ 's ID is: %@", service.name, service.uuid);
                if ([service.name isEqualToString:@"devinfo"]) {
                    __weak WLDeviceInfoService *thisService = (WLDeviceInfoService *)service;
                    [service discoverCharacteristics:[service characteristics] withBlock:^(NSDictionary *chDict, NSError *error) {
                        [thisService readDeviceInfo];
                    }];
                    
                }else if ([service.name isEqualToString:@"battery"]){
                    __weak WLBatteryService *thisService = (WLBatteryService *)service;
                    [service discoverCharacteristics:[service characteristics] withBlock:^(NSDictionary *chDict, NSError *error) {
                        [thisService readBatteryInfo];
                    }];
                    
                }
//                    else if ([service.name isEqualToString:@"barometer"]){
//                    __weak WLBarometerService * barometerService = (WLBarometerService *)service;
//                    [service discoverCharacteristics:[service characteristics] withBlock:^(NSDictionary *chDict, NSError *error) {
//                        [barometerService turnOn];
//                        [barometerService requestCalibration];
//                    }];
//
//                }else if ([service.name isEqualToString:@"humidity"]){
//                    __weak WLHumidityService * humService = (WLHumidityService *)service;
//                    [service discoverCharacteristics:[service characteristics] withBlock:^(NSDictionary *chDict, NSError *error) {
//                        [humService turnOn];
//                                           }];
//
//                }else if ([service.name isEqualToString:@"uvindex"]){
//                    __weak WLUVIndexService * uvService = (WLUVIndexService *)service;
//                    [service discoverCharacteristics:[service characteristics] withBlock:^(NSDictionary *chDict, NSError *error) {
//                        
//                        [uvService turnOn];
//                    }];
//
//                }
                    else if ([service.name isEqualToString:@"historydata"]){
                    __weak WLHistoryDataService *thisService = (WLHistoryDataService *)service;
                    [service discoverCharacteristics:[service characteristics] withBlock:^(NSDictionary *chDict, NSError *error) {
                        [thisService setTheWeatherLog:self];
                        [thisService performSelector:@selector(turnOn) withObject:nil afterDelay:10];
//                        [thisService performSelector:@selector(setTheWeatherLog) withObject:nil afterDelay:10];
//                        [thisService turnOn];
                    }];
                    
                    }else if([service.name isEqualToString:@"oad"]){
                        __weak WLOADService *thisService = (WLOADService *)service;
                        [service discoverCharacteristics:[service characteristics] withBlock:^(NSDictionary *chDict, NSError *error) {
//                            [thisService requestCalibration];
                            [thisService turnOn];
                            NSData *data;
                            
                            [thisService performSelector:@selector(pushImage:) withObject:data afterDelay:10];
                        }];
                        
                    }else if([service.name isEqualToString:@"monitor"]){
                        __weak WLMonitorService *thisService = (WLMonitorService *)service;
                        [service discoverCharacteristics:[service characteristics] withBlock:^(NSDictionary *chDict, NSError *error) {
                            [thisService requestCalibration];
                            [thisService turnOn];
                        }];
                    }
                
//                    else{
//                    __weak WLBaseService *thisService = (WLBaseService *)service;
//                    [service discoverCharacteristics:[service characteristics] withBlock:^(NSDictionary *chDict, NSError *error) {
//                        [thisService turnOn];
//                        
//                        if ([service.name isEqualToString:@"barometer"]) {
//                            __weak WLBarometerService *baroService = (WLBarometerService *)service;
//                            [baroService requestCalibration];
//                        }
                
//                        for (NSString *key in chDict) {
//                            YMSCBCharacteristic *ct = chDict[key];
                        
                            //NSLog(@"%@ %@ %@", ct, ct.cbCharacteristic, ct.uuid);
                            
//                            [ct discoverDescriptorsWithBlock:^(NSArray *ydescriptors, NSError *error) {
//                                if (error) {
//                                    return;
//                                }
//                                for (YMSCBDescriptor *yd in ydescriptors) {
//                                    NSLog(@"Descriptor: %@ %@ %@", thisService.name, yd.UUID, yd.cbDescriptor);
//                                }
//                            }];
                        //}
//                    }];
//                }
            }
        }];
    }];
}


//- (WLHumidityService *)humidity {
//    return self.serviceDict[@"humidity"];
//}
//
- (WLDeviceInfoService *)devinfo {
    return self.serviceDict[@"devinfo"];
}

//- (WLBarometerService *)barometer {
//    return self.serviceDict[@"barometer"];
//}

//- (WLUVIndexService *)uvindex {
//    return self.serviceDict[@"uvindex"];
//}

- (WLDeviceInfoService *)battery {
    return self.serviceDict[@"battery"];
}

- (WLHistoryDataService *)historydata {
    return self.serviceDict[@"historydata"];
}

- (WLMonitorService *)monitorService{
    return self.serviceDict[@"monitor"];
}

- (WLOADService *)oadService{
    return self.serviceDict[@"oad"];
}


@end
