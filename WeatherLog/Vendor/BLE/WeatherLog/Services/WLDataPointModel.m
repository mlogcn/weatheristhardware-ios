//
//  WLDataPointModel.m
//  WeatherLog
//
//  Created by 龙恒舟 on 15/9/14.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import "WLDataPointModel.h"

@implementation WLDataPointModel

- (instancetype)initWithData:(NSInteger) measuredAt
                      humidity:(double)hum
                      barometer:(double)baro
                 temperature:(double)temp
                   uvi:(double)uvi
                   lat:(double)lat
                   lon:(double)lon{
    
    self = [super init];
    if (self) {
        _measuredTime = measuredAt;
        _createdTime = [[NSDate date] timeIntervalSince1970];
        _humidity = hum;
        _barometer = baro;
        _temperature = temp;
        _uvi = uvi;
        _lat = lat;
        _lon = lon;
    }
    return self;
}

- (instancetype)init {
    
    self = [super init];
    if (self) {
        _measuredTime = 0;
        _createdTime = [[NSDate date] timeIntervalSince1970];
        _humidity = 0.0;
        _barometer = 0.0;
        _temperature = 0.0;
        _uvi = 0.0;
        _lat = 0.0;
        _lon = 0.0;
    }
    return self;
}

- (void) setRecord:(NSInteger) measuredAt
          humidity:(double)hum
         barometer:(double)baro
       temperature:(double)temp
               uvi:(double)uvi {
    
    _measuredTime = measuredAt;
    _humidity = hum;
    _barometer = baro;
    _temperature = temp;
    _uvi = uvi;
    
}

- (void) setLocation:(double) lat lon:(double) lon {
    _lat = lat;
    _lon = lon;
}
@end
