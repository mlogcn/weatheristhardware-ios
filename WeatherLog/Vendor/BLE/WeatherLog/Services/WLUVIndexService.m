//
//  WLUVIndexService.m
//  WeatherLog
//
//  Created by 龙恒舟 on 15/8/3.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import "WLUVIndexService.h"
#import "YMSCBCharacteristic.h"

@interface WLUVIndexService ()

@property (nonatomic, strong) NSNumber *visibleLight;
@property (nonatomic, strong) NSNumber *infraredLight;
@property (nonatomic, strong) NSNumber *UVI;

@end

@implementation WLUVIndexService

- (instancetype)initWithName:(NSString *)oName
                      parent:(YMSCBPeripheral *)pObj
                      baseHi:(int64_t)hi
                      baseLo:(int64_t)lo
               serviceOffset:(int)serviceOffset {
    
    self = [super initWithName:oName
                        parent:pObj
                        baseHi:hi
                        baseLo:lo
                 serviceOffset:serviceOffset];
    
    if (self) {
        [self addCharacteristic:@"data" withOffset:kWeatherLog_UVINDEX_DATA];
        [self addCharacteristic:@"config" withOffset:kWeatherLog_UVINDEX_CONFIG];

    }
    return self;
}

- (void)notifyCharacteristicHandler:(YMSCBCharacteristic *)yc error:(NSError *)error {
    if (error) {
        return;
    }
    
    if ([yc.name isEqualToString:@"data"]) {
        NSData *data = yc.cbCharacteristic.value;
        
        char val[data.length];
        [data getBytes:&val length:data.length];
        
        uint16_t v0 = val[0];
        uint16_t v1 = val[1];
        uint16_t v2 = val[2];
        uint16_t v3 = val[3];
        uint16_t v4 = val[4];
        uint16_t v5 = val[5];
        
        uint16_t xx = yms_u16_build(v1, v0);
        uint16_t yy = yms_u16_build(v3, v2);
        uint16_t zz = yms_u16_build(v5, v4);
        
        double uvi = xx / 100.0;
        double visible = yy / 100.0;
        double infrared = zz / 100.0;
        
        __weak WLUVIndexService *this = self;
        _YMS_PERFORM_ON_MAIN_THREAD(^{
            [self willChangeValueForKey:@"sensorValues"];
            this.UVI = @(uvi);
            this.visibleLight = @(visible);
            this.infraredLight = @(infrared);
            [self didChangeValueForKey:@"sensorValues"];
        });
        
        
    }
//    else if ([yc.name isEqualToString:@"config"]) {
//    }
    
}


- (NSDictionary *)sensorValues
{
    return @{ @"visibleLight": self.visibleLight,
              @"infraredLight": self.infraredLight,
              @"UVI": self.UVI };
}

@end
