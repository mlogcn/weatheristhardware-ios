//
//  WLDataPointModel.h
//  WeatherLog
//
//  Created by 龙恒舟 on 15/9/14.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WLDataPointModel : NSObject

@property (assign, readonly) NSInteger measuredTime;

@property (assign, readonly) NSInteger createdTime;

@property (assign, readonly) double humidity;

@property (assign, readonly) double barometer;

@property (assign, readonly) double temperature;

@property (assign, readonly) double uvi;

@property (assign, readonly) double lat;

@property (assign, readonly) double lon;

- (void) setRecord:(NSInteger) measuredAt
          humidity:(double)hum
         barometer:(double)baro
       temperature:(double)temp
               uvi:(double)uvi;

- (void) setLocation:(double) lat lon:(double) lon;

@end
