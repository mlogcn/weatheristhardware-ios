//
//  WLHistoryDataService.h
//  WeatherLog
//
//  Created by 龙恒舟 on 15/8/26.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import "YMSCBService.h"
#import "WeatherLog.h"
#import "WLDataPointModel.h"
#import "WLWeatherLog.h"

@protocol HistoryDelegate <NSObject>

- (void)readAllHistoryData;

@end

@interface WLHistoryDataService : YMSCBService

/// record amount    read/notify
@property (nonatomic, strong, readonly) NSNumber *recordCount;

// The latest record data read by WeatherLog    read/notify
@property (nonatomic, strong, readonly) NSNumber *currentRecord;

// The current pointer
@property (nonatomic, strong, readonly) NSNumber *rptr;

// The readed data
@property (nonatomic, strong, readonly) NSMutableArray *dataReaded;

@property (nonatomic, strong, readonly) WLWeatherLog *weatherLog;

//设置count为notify, 设置record为notify, config写0x1
- (void) turnOn;

//rptr写++，然后赋值给rptr
- (void) setNextValue;

// 是否读取完毕
- (BOOL) hasReadCompleted;

// 读取所有数据
- (void) readAllData;

- (void) setTheWeatherLog: (WLWeatherLog *) weatherLog;

@property (nonatomic, assign) id<HistoryDelegate>delegate;

@end
