//
//  WLBatteryLevelService.m
//  WeatherLog
//
//  Created by 龙恒舟 on 15/8/25.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import "WLBatteryService.h"
#import "YMSCBCharacteristic.h"

@interface WLBatteryService ()

@property (nonatomic, strong) NSNumber *batteryLevel;

@end

@implementation WLBatteryService


- (instancetype)initWithName:(NSString *)oName
                      parent:(YMSCBPeripheral *)pObj
                      baseHi:(int64_t)hi
                      baseLo:(int64_t)lo
               serviceOffset:(int)serviceOffset {
    
    self = [super initWithName:oName
                        parent:pObj
                        baseHi:hi
                        baseLo:lo
                 serviceOffset:serviceOffset];
    
    if (self) {
        [self addCharacteristic:@"Battery Level" withAddress:kWeatherLog_BATTERY_LEVEL];
    }
    return self;
}

- (void)readBatteryInfo{
    
    YMSCBCharacteristic *batteryLevelCt = self.characteristicDict[@"Battery Level"];
    
    __weak WLBatteryService *this = self;
    
    [batteryLevelCt readValueWithBlock:^(NSData *data, NSError *error) {
        if (error) {
            NSLog(@"ERROR: %@", error);
            return;
        }
        
        char val[data.length];
        [data getBytes:&val length:data.length];
        uint16_t v0 = val[0];
        int level = (int)v0;
 
        _YMS_PERFORM_ON_MAIN_THREAD(^{
            [self willChangeValueForKey:@"batteryLevel"];
            this.batteryLevel = @(level);
            [self didChangeValueForKey:@"batteryLevel"];
            
            NSLog(@"battery level: %d", level);
        });
    }];
    
    [batteryLevelCt setNotifyValue:YES withBlock:^(NSError *error) {
        NSLog(@"Data notification for %@ on", this.name);
        
    }];
    
}

- (void)notifyCharacteristicHandler:(YMSCBCharacteristic *)yc error:(NSError *)error {
    if (error) {
        
        NSLog(@"ERROR: %@", error);
        return;
    }
    
    if ([yc.name isEqualToString:@"Battery Level"]) {
        NSData *data = yc.cbCharacteristic.value;
        
        char val[data.length];
        [data getBytes:&val length:data.length];
        uint16_t v0 = val[0];
        double level = (double)v0;
        
        __weak WLBatteryService *this = self;
        _YMS_PERFORM_ON_MAIN_THREAD(^{
            [self willChangeValueForKey:@"batteryLevel"];
            this.batteryLevel = @(level);
            [self didChangeValueForKey:@"batteryLevel"];
        });
    }
}


@end
