//
//  WLUVIndexService.h
//  WeatherLog
//
//  Created by 龙恒舟 on 15/8/3.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import "WLBaseService.h"

@interface WLUVIndexService : WLBaseService

@property (nonatomic, strong, readonly) NSDictionary *sensorValues;

/// Ambient visible light
@property (nonatomic, strong, readonly) NSNumber *visibleLight;

/// Ambient infrared light
@property (nonatomic, strong, readonly) NSNumber *infraredLight;

/// Ultra Violet Index
@property (nonatomic, strong, readonly) NSNumber *UVI;

@end
