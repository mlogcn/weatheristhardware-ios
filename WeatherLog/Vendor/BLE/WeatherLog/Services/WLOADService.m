//
//  WLOADService.m
//  WeatherLog
//
//  Created by 龙恒舟 on 15/10/21.
//  Copyright © 2015年 龙恒舟. All rights reserved.
//

#import "WLOADService.h"

#import "YMSCBCharacteristic.h"
#import "YMSCBUtils.h"
#import "WeatherLog.h"
#import "WLWeatherLogUtils.h"
#import "oad.h"

@interface WLOADService ()

@property (retain, nonatomic) NSTimer *imageDetectTimer;

@property (assign, nonatomic) uint16_t imgVersion;

@property (assign, nonatomic) BOOL start;

@property (assign, nonatomic) BOOL inProgramming;

@property (retain, nonatomic) NSData *imageFile;

@property (assign, nonatomic) BOOL cancled;

@property (assign ,nonatomic) int nBlocks ;

@property (assign, nonatomic) int nBytes;

@property (assign, nonatomic) int iBlocks;

@property (assign, nonatomic) int iBytes;


@property (nonatomic, strong) NSNumber *theProgress;

@property (nonatomic, strong) NSNumber *secondsPerBlock;

@property (nonatomic, strong) NSNumber *secondsLeft;

@end


@implementation WLOADService


@synthesize imageDetectTimer = _imageDetectTimer;

@synthesize imgVersion = _imgVersion;

@synthesize start = _start;

@synthesize inProgramming = _inProgramming;

@synthesize imageFile = _imageFile;

@synthesize cancled = _cancled;

@synthesize nBlocks = _nBlocks;

@synthesize nBytes = _nBytes;

@synthesize iBlocks = _iBlocks;

@synthesize iBytes = _iBytes;



- (instancetype)initWithName:(NSString *)oName
                      parent:(YMSCBPeripheral *)pObj
                      baseHi:(int64_t)hi
                      baseLo:(int64_t)lo
               serviceOffset:(int)serviceOffset {
    
    self = [super initWithName:oName
                        parent:pObj
                        baseHi:hi
                        baseLo:lo
                 serviceOffset:serviceOffset];
    
    if (self) {
        yms_u128_t pbase = self.base;
        self.uuid = [YMSCBUtils createCBUUID:&pbase withIntBLEOffset:serviceOffset];
        
        [self addCharacteristic:@"notify" withOffset:kWeatherLog_OAD_IMAGE_NOTIFY_UUID];
        
        [self addCharacteristic:@"block" withOffset:kWeatherLog_OAD_IMAGE_BLOCK_REQUEST_UUID];
    }
    
    self.imgVersion = 0xFFFF;
    self.start = YES;
    _secondsLeft = @(0.0);
    _secondsPerBlock = @(0.09/4);
    _theProgress = @(0.0);
    
    return self;
}

- (void)addCharacteristic:(NSString *)cname withOffset:(int)addrOffset {
    YMSCBCharacteristic *yc;
    
    yms_u128_t pbase = self.base;
    
    CBUUID *uuid = [YMSCBUtils createCBUUID:&pbase withIntBLEOffset:addrOffset];
    
    yc = [[YMSCBCharacteristic alloc] initWithName:cname
                                            parent:self.parent
                                              uuid:uuid
                                            offset:addrOffset];
    
    self.characteristicDict[cname] = yc;
}

- (void)turnOn {
    __weak WLOADService *this = self;
    
    YMSCBCharacteristic *notifyCT = self.characteristicDict[@"notify"];
    [notifyCT setNotifyValue:YES withBlock:^(NSError *error) {
        NSLog(@"Data notification for %@ on", this.name);
        
    }];
    [notifyCT writeByte:0x00 withBlock:^(NSError *error) {
        if (error) {
            NSLog(@"ERROR: %@", error);
            return;
        }
        NSLog(@"TURNED ON Image A: %@", this.name);
    }];
    
    self.imageDetectTimer = [NSTimer scheduledTimerWithTimeInterval:1.5f target:self selector:@selector(imageDetectTimerTick:) userInfo:nil repeats:NO];
    self.imgVersion = 0xFFFF;
    self.start = YES;
    
    _YMS_PERFORM_ON_MAIN_THREAD(^{
        this.isOn = YES;
    });
}

-(void) imageDetectTimerTick:(NSTimer *)timer {
    //IF we have come here, the image userID is B.
    __weak WLOADService *this = self;
    
    YMSCBCharacteristic *notifyCT = self.characteristicDict[@"notify"];
    [notifyCT writeByte:0x01 withBlock:^(NSError *error) {
        if (error) {
            NSLog(@"ERROR: %@", error);
            return;
        }
        NSLog(@"TURNED ON Image B: %@", this.name);
    }];
}

- (void)notifyCharacteristicHandler:(YMSCBCharacteristic *)yc error:(NSError *)error {
    
    if (error) {
        return;
    }
//    CBCharacteristic * characteristic = (CBCharacteristic *)yc;
    
    if ([yc.name isEqualToString:@"notify"]) {
        
        NSLog(@"%@",yc.cbCharacteristic.value);
        if (self.imgVersion == 0xFFFF) {
            unsigned char data[yc.cbCharacteristic.value.length];
            
            [yc.cbCharacteristic.value getBytes:&data length:yc.cbCharacteristic.value.length];
            
            self.imgVersion = ((uint16_t)data[1] << 8 & 0xff00) | ((uint16_t)data[0] & 0xff);
            
            NSLog(@"self.imgVersion : %04hx",self.imgVersion);
        }
        NSLog(@"OAD Image notify : %@",yc.cbCharacteristic.value);
    }
}

- (BOOL)pushImage:(NSData *) imageFile
{
    BOOL result = NO;
    
    if ([self isCorrectImage])
    {
        [self uploadImage];
    }
    return result;
}

-(BOOL) isCorrectImage
{
    unsigned char imageFileData[self.imageFile.length];
    //[self.imageFile getBytes:imageFileData];
    [self.imageFile getBytes:imageFileData length:_nBytes];
    
    img_hdr_t imgHeader;
    memcpy(&imgHeader, &imageFileData[0 + OAD_IMG_HDR_OSET], sizeof(img_hdr_t));
    //    memcpy(<#dest#>, <#src#>, <#len#>)
    
    if ((imgHeader.ver & 0x01) != (self.imgVersion & 0x01))
        return YES;
    return NO;
}


- (void)uploadImage
{
    self.inProgramming = YES;
    self.cancled = NO;
    
    unsigned char imageFileData[self.imageFile.length];
    [self.imageFile getBytes:imageFileData length:self.imageFile.length];
    uint8_t requestData[OAD_IMG_HDR_SIZE + 2 + 2];  //12 bytes
    
    for (int i=0; i<20; i++) {
        NSLog(@"%02hhx",imageFileData[i]);
    }
    
    img_hdr_t imgHeader;
    memcpy(&imgHeader, &imageFileData[0 + OAD_IMG_HDR_OSET], sizeof(img_hdr_t));
    
    requestData[0] = LO_UINT16(imgHeader.ver);
    requestData[1] = HI_UINT16(imgHeader.ver);
    
    requestData[2] = LO_UINT16(imgHeader.len);
    requestData[3] = HI_UINT16(imgHeader.len);
    
    requestData[OAD_IMG_HDR_SIZE + 0] = LO_UINT16(12);
    requestData[OAD_IMG_HDR_SIZE + 1] = HI_UINT16(12);
    
    requestData[OAD_IMG_HDR_SIZE + 2] = LO_UINT16(15);
    requestData[OAD_IMG_HDR_SIZE + 3] = HI_UINT16(15);
    
//    CBUUID *sUUID = [CBUUID UUIDWithString:OAD_SERVICE_UUID];
//    CBUUID *cUUID = [CBUUID UUIDWithString:OAD_IMAGE_NOTIFY_UUID];
//
    
    YMSCBCharacteristic *notifyCT = self.characteristicDict[@"notify"];
    [notifyCT writeValue:[NSData dataWithBytes:requestData length:(OAD_IMG_HDR_SIZE +2 +2)] withBlock:^(NSError *error) {
        if (error) {
            NSLog(@"ERROR: %@", error);
            return;
        }
        NSLog(@"Have writtn the request data to the notify cha");
    }];
    
    
//    [HJYBLE writeCharacteristic:self.peripheral sCBUUID:sUUID cCBUUID:cUUID data:[NSData dataWithBytes:requestData length:(OAD_IMG_HDR_SIZE + 2 +2)]];
    
    self.nBlocks = imgHeader.len / (OAD_BLOCK_SIZE / HAL_FLASH_WORD_SIZE);
    self.nBytes = imgHeader.len * HAL_FLASH_WORD_SIZE;
    
    self.iBlocks = 0;
    self.iBytes = 0;
    
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(programmingTimerTick:) userInfo:nil repeats:NO];
    
}

- (void)programmingTimerTick:(NSTimer *)timer
{
    if (self.cancled) {
        self.cancled = FALSE;
        return;
    }
    
    unsigned char imageFileData[self.imageFile.length];
    [self.imageFile getBytes:imageFileData length:self.imageFile.length];
    
    uint8_t requestData[2 + OAD_BLOCK_SIZE];
    
    // This block is run 4 times, this is needed to get CoreBluetooth to send consequetive packets in the same connection interval.
    for (int i=0; i<4; i++) {
        requestData[0] = LO_UINT16(self.iBlocks);
        requestData[1] = HI_UINT16(self.iBlocks);
        
        memcpy(&requestData[2], &imageFileData[self.iBytes], OAD_BLOCK_SIZE);
        
//        CBUUID *sUUID = [CBUUID UUIDWithString:OAD_SERVICE_UUID];
//        CBUUID *cUUID = [CBUUID UUIDWithString:OAD_IMAGE_BLOCK_REQUEST_UUID];
//        
//        [HJYBLE writeNoResponseCharacteristic:self.peripheral sCBUUID:sUUID cCBUUID:cUUID data:[NSData dataWithBytes:requestData length:(2 + OAD_BLOCK_SIZE)]];
        
        YMSCBCharacteristic *notifyCT = self.characteristicDict[@"block"];
        [notifyCT writeValue:[NSData dataWithBytes:requestData length:(2 + OAD_BLOCK_SIZE)] withBlock:^(NSError *error) {
        }];
        
        
        self.iBlocks ++;
        self.iBytes += OAD_BLOCK_SIZE;
        
        //        NSLog(@"iBlocks %d  iBytes %d : %@",self.iBlocks, self.iBytes,[NSData dataWithBytes:requestData length:(2 + OAD_BLOCK_SIZE)]);
        
        if (self.iBlocks == self.nBlocks) {
            
            self.inProgramming = NO;
            
//            [self completionDialog];
            
            return;
        }
        else
        {
            if (i == 3) {
                [NSTimer scheduledTimerWithTimeInterval:0.09 target:self selector:@selector(programmingTimerTick:) userInfo:nil repeats:NO];
            }
        }
        
    }
    
    float pro = (float)((float)self.iBlocks / (float)self.nBlocks);
    
//    self.progressViewController.progressBar.progress = pro;
//    self.progressViewController.progressLabel.text = [NSString stringWithFormat:@"%0.1f%%",pro * 100.0f];
    
    float secondsPerBlock = 0.09 / 4;
    
    
    float secondsLeft = (float)(self.nBlocks - self.iBlocks) * secondsPerBlock;
    
    __weak WLOADService *this = self;
    
    _YMS_PERFORM_ON_MAIN_THREAD(^{
    
        [self willChangeValueForKey:@"progressValues"];
        this.secondsLeft = @(secondsLeft);
        this.secondsPerBlock = @(secondsPerBlock);
        this.theProgress = @(pro);
        [self didChangeValueForKey:@"progressValues"];
        
    });
//    self.progressViewController.progressBar.progress = pro;
//    self.progressViewController.progressLabel.text = [NSString stringWithFormat:@"%0.1f%%",pro * 100.0f];
//    self.progressViewController.timeLabel.text = [NSString stringWithFormat:@"Time remaining : %d:%02d",(int)(secondsLeft / 60), (int)secondsLeft - (int)(secondsLeft / 60) * (int)60];
    
    if (self.start) {
        self.start = NO;
//        if (!self.progressViewController) {
//            self.progressViewController = [[HJYProgressViewController alloc] init];
//        }
//        [self presentViewController:self.progressViewController animated:YES completion:^{}];
    }
    
}

- (NSDictionary *)progressValues
{
    return @{ @"theProgress": self.theProgress,
              @"secondsLeft": self.secondsLeft,
              @"secondsPerBlock": self.secondsPerBlock};
}

@end
