//
//  WLDeviceInfoService.h
//  WeatherLog
//
//  Created by 龙恒舟 on 15/8/3.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import "WLBaseService.h"
#import "WeatherLog.h"

@interface WLDeviceInfoService : YMSCBService

/** @name Properties */
/// System ID
@property (nonatomic, strong) NSString *system_id;
/// Model Number
@property (nonatomic, strong) NSString *model_number;
/// Serial Number
@property (nonatomic, strong) NSString *serial_number;
/// Firmware Revision
@property (nonatomic, strong) NSString *firmware_rev;
/// Hardware Revision
@property (nonatomic, strong) NSString *hardware_rev;
/// Software Revision
@property (nonatomic, strong) NSString *software_rev;
/// Manufacturer Name
@property (nonatomic, strong) NSString *manufacturer_name;
/// IEEE 11073 Certification Data
@property (nonatomic, strong) NSString *ieee11073_cert_data;


- (void)readDeviceInfo;
@end
