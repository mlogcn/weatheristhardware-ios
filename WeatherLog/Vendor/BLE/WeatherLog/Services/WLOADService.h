//
//  WLOADService.h
//  WeatherLog
//
//  Created by 龙恒舟 on 15/10/21.
//  Copyright © 2015年 龙恒舟. All rights reserved.
//

#import "YMSCBService.h"

@interface WLOADService : YMSCBService

@property (nonatomic, readonly) NSDictionary *progressValues;

- (void)turnOn;

- (BOOL)pushImage:(NSData *) imageFile;

@end
