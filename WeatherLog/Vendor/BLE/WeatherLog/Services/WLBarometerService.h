//
//  WLBarometerService.h
//  WeatherLog
//
//  Created by 龙恒舟 on 15/8/3.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import "WLBaseService.h"

@interface WLBarometerService : WLBaseService


/**
 Inherited property of WLBaseService.
 Keys: @"pressure", @"ambientTemp".
 */
@property (nonatomic, strong, readonly) NSDictionary *sensorValues;

/// Pressure measurement
@property (nonatomic, strong, readonly) NSNumber *pressure;

/// Ambient temperature measurement
@property (nonatomic, strong, readonly) NSNumber *ambientTemp;


@property (nonatomic, assign) BOOL isCalibrating;

@property (nonatomic, assign) BOOL isCalibrated;


/**
 Request calibration of barometer.
 */
- (void)requestCalibration;


@end
