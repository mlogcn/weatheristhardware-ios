//
//  WLHistoryDataService.m
//  WeatherLog
//
//  Created by 龙恒舟 on 15/8/26.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import "WLHistoryDataService.h"
#import "YMSCBCharacteristic.h"
#import "YMSCBUtils.h"
#import "WLMonitorService.h"

@interface WLHistoryDataService ()

/// record amount    read/notify
@property (nonatomic, strong) NSNumber *recordCount;

// The latest record data read by WeatherLog    read/notify
@property (nonatomic, strong) NSNumber *currentRecord;

// The readed data
@property (nonatomic, strong) NSMutableArray *dataReaded;

@property (nonatomic, strong) NSNumber *rptr;

@property (nonatomic, strong) WLWeatherLog *weatherLog;



@end


@implementation WLHistoryDataService

- (instancetype)initWithName:(NSString *)oName
                      parent:(YMSCBPeripheral *)pObj
                      baseHi:(int64_t)hi
                      baseLo:(int64_t)lo
               serviceOffset:(int)serviceOffset {
    
    self = [super initWithName:oName
                        parent:pObj
                        baseHi:hi
                        baseLo:lo
                 serviceOffset:serviceOffset];
    
    if (self) {
        yms_u128_t pbase = self.base;
        self.uuid = [YMSCBUtils createCBUUID:&pbase withIntBLEOffset:serviceOffset];
        
        [self addCharacteristic:@"set" withOffset:kWeatherLog_HISTORYDATA_SET];
        [self addCharacteristic:@"config" withOffset:kWeatherLog_HISTORYDATA_CONFIG];
        [self addCharacteristic:@"count" withOffset:kWeatherLog_HISTORYDATA_COUNT];
        [self addCharacteristic:@"rptr" withOffset:kWeatherLog_HISTORYDATA_RPTR];
        [self addCharacteristic:@"record" withOffset:kWeatherLog_HISTORYDATA_RECORD];
    
    }
    self.dataReaded = [NSMutableArray array];
    return self;
}

- (void)addCharacteristic:(NSString *)cname withOffset:(int)addrOffset {
    YMSCBCharacteristic *yc;
    
    yms_u128_t pbase = self.base;
    
    CBUUID *uuid = [YMSCBUtils createCBUUID:&pbase withIntBLEOffset:addrOffset];
    
    yc = [[YMSCBCharacteristic alloc] initWithName:cname
                                            parent:self.parent
                                              uuid:uuid
                                            offset:addrOffset];
    
    self.characteristicDict[cname] = yc;
}
- (void)writeToConfig{
      __weak WLHistoryDataService *this = self;
    YMSCBCharacteristic *configCt = self.characteristicDict[@"config"];
    [configCt writeByte:0x1 withBlock:^(NSError *error) {
        if (error) {
            NSLog(@"ERROR write 0x1 to config: %@", error);
            return;
        }
        NSLog(@"TURNED ON: %@", this.name);
    }];

}
- (void)turnOn {
    __weak WLHistoryDataService *this = self;
    
    //set count notified
    YMSCBCharacteristic *countCt = self.characteristicDict[@"count"];
    [countCt setNotifyValue:YES withBlock:^(NSError *error) {
        NSLog(@"Count notification for %@ on", this.name);
        if (error) {
            NSLog(@"ERROR set count notified: %@", error);
            return;
        }
    }];
    
    
    //write 0x1 to config
    YMSCBCharacteristic *configCt = self.characteristicDict[@"config"];
    [configCt writeByte:0x1 withBlock:^(NSError *error) {
        if (error) {
            NSLog(@"ERROR write 0x1 to config: %@", error);
            return;
        }
        NSLog(@"TURNED ON: %@", this.name);
    }];
    
    _YMS_PERFORM_ON_MAIN_THREAD(^{
        this.isOn = YES;
    });
}

- (void) setNextValue {
    NSLog(@"next= %@",[NSDate date]);
    YMSCBCharacteristic *rptrCt = self.characteristicDict[@"rptr"];
    uint16_t theRptr = [self.rptr intValue];
    
    if (theRptr >= self.recordCount.intValue) {
        return;
    }
    
    NSData *data = [YMSCBUtils int16ToData:theRptr];
    
    //__weak WLHistoryDataService *this = self;

    [rptrCt writeValue:data withBlock:^(NSError *error) {
        if (error) {
            NSLog(@"ERROR setNextValue: %@", error);
            return;
        }
        NSLog(@"write = %@",[NSDate date]);
    }];
    NSLog(@"Set the next value: %d", theRptr);
    
}


- (void)notifyCharacteristicHandler:(YMSCBCharacteristic *)yc error:(NSError *)error {
    NSLog(@"notify=%@",[NSDate date]);
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        if (error) {
            return;
        }
        
        __weak WLHistoryDataService *this = self;
        
        if ([yc.name isEqualToString:@"count"]) {
            NSData *data = yc.cbCharacteristic.value;
            char val[data.length];
            [data getBytes:&val length:data.length];
            
            uint16_t v0 = val[0];
            uint16_t v1 = val[1];
            uint16_t v2 = val[2];
            uint16_t v3 = val[3];
            
            //        uint16_t nCount_lo = yms_u16_build(v0, v1);
            //        uint16_t nCount_hi = yms_u16_build(v2, v3);
            
            uint32_t nCount = yms_u32_build(v0, v1, v2, v3);
//            if (nCount >= 50) {
//                nCount = 50;
//            }
            self.recordCount = @(nCount);
            
            NSLog(@"The total count of records is: %u", nCount);
            
            [self readAllData];
            
        }
        else if ([yc.name isEqualToString:@"record"]){
            
            while (_weatherLog.monitorService == NULL) {
                [NSThread sleepForTimeInterval:1.0];
            }
            WLMonitorService *tempMonitorService = _weatherLog.monitorService;
            NSData *data = yc.cbCharacteristic.value;
            
            double pressure = 0.0;
            double temp = 0.0;
            double humidity = 0.0;
            double uvi = 0.0;
            
            double pressure1 = 0.0;
            double temp1 = 0.0;
            double humidity1 = 0.0;
            double uvi1 = 0.0;
            
            [tempMonitorService calcAllMonitorData1:data pressure:&pressure temp:&temp humidity:&humidity uvi:&uvi pressure1:&pressure1 temp1:&temp1 humidity1:&humidity1 uvi1:&uvi1];
            
            NSLog(@"The data length is %ld", data.length);
            char val[data.length];
            [data getBytes:&val length:data.length];
            
            NSMutableString* hexString = [NSMutableString string];
            for (int i=0; i<data.length; i++) {
                [hexString appendFormat:@"%02x", val[i]];
            }
            
            WLDataPointModel *model = [[WLDataPointModel alloc] init];
            int time = [[NSDate date] timeIntervalSince1970];
//            double values[4];
//            for (int i=1; i<5; i++) {
//                uint16_t v0 = val[i*4];
//                uint16_t v1 = val[i*4 + 1];
//                uint16_t v2 = val[i*4 + 2];
//                uint16_t v3 = val[i*4 + 3];
//                uint32_t value = yms_u32_build(v0, v1, v2, v3);
//                values[i-1] = value;
//            }
            
            [model setRecord:time humidity:humidity barometer:pressure temperature:temp uvi:uvi];
            [self.dataReaded addObject:model];
            
            
            [model setRecord:time humidity:humidity1 barometer:pressure1 temperature:temp1 uvi:uvi1];
            [self.dataReaded addObject:model];
            
            NSLog(@"The %u data is: %@", self.rptr.intValue, hexString);
            NSLog(@"The values are: %f   %f   %f   %f", pressure, temp, humidity, uvi);
            NSLog(@"The values1 are: %f   %f   %f   %f", pressure1, temp1, humidity1, uvi1);
            
            uint32_t thecount = [self.rptr intValue] + 1;
            
            this.rptr = @(thecount);
            [self setNextValue];
        }
    });
}

//是否读取完毕
- (BOOL) hasReadCompleted {
    BOOL bResult = TRUE;
    if (self.dataReaded.count != self.recordCount.intValue) {
        bResult = FALSE;
    }
    return bResult;
}

- (void) readAllData {
    __weak WLHistoryDataService *this = self;
    
    if (self.recordCount.intValue != 0) {
        
        //set record notified
        YMSCBCharacteristic *recordCt = self.characteristicDict[@"record"];
        [recordCt setNotifyValue:YES withBlock:^(NSError *error) {
            NSLog(@"Record notification for %@ on", this.name);
            if (error) {
                NSLog(@"ERROR set record notified: %@", error);
                return;
            }
        }];
        self.rptr = @(0);
        [self setNextValue];
    }
}

- (void) setTheWeatherLog: (WLWeatherLog *) weatherLog {
    _weatherLog = weatherLog;
}

@end
