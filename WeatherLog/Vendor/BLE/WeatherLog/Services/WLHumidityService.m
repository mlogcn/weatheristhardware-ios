//
//  WLHumidityService.m
//  WeatherLog
//
//  Created by 龙恒舟 on 15/8/3.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import "WLHumidityService.h"
#import "WeatherLog.h"
#import "YMSCBCharacteristic.h"
#import "WLWeatherLogUtils.h"

//double calcHumTmp(uint16_t rawT) {
//    double v;
//    v = -46.85 + 175.72/65536 *(double)rawT;
//    return v;
//}
//
//double calcHumRel(uint16_t rawH) {
//    double v;
//    v = -6.0 + (125.0/65536.0) * (double)rawH;
//    return v;
//}

@interface WLHumidityService ()

@property (nonatomic, strong) NSNumber *ambientTemp;
@property (nonatomic, strong) NSNumber *relativeHumidity;

@end

@implementation WLHumidityService

- (instancetype)initWithName:(NSString *)oName
                      parent:(YMSCBPeripheral *)pObj
                      baseHi:(int64_t)hi
                      baseLo:(int64_t)lo
               serviceOffset:(int)serviceOffset {
    
    self = [super initWithName:oName
                        parent:pObj
                        baseHi:hi
                        baseLo:lo
                 serviceOffset:serviceOffset];
    
    if (self) {
        [self addCharacteristic:@"data" withOffset:kWeatherLog_HUMIDITY_DATA];
        [self addCharacteristic:@"config" withOffset:kWeatherLog_HUMIDITY_CONFIG];
    }
    return self;
    
}


- (void)notifyCharacteristicHandler:(YMSCBCharacteristic *)yc error:(NSError *)error {
    if (error) {
        return;
    }
    
    if ([yc.name isEqualToString:@"data"]) {
        NSData *data = yc.cbCharacteristic.value;
        
        char val[data.length];
        
        [data getBytes:&val length:data.length];
        
        uint16_t v0 = val[0];
        uint16_t v1 = val[1];
        uint16_t v2 = val[2];
        uint16_t v3 = val[3];
        
        uint16_t rawTemperature = yms_u16_build(v0, v1);
        uint16_t rawHumidity = yms_u16_build(v2, v3);
        
        __weak WLHumidityService *this = self;
        _YMS_PERFORM_ON_MAIN_THREAD(^{
            [self willChangeValueForKey:@"sensorValues"];
            this.ambientTemp = @(calcHumTmp(rawTemperature));
            this.relativeHumidity = @(calcHumRel(rawHumidity));
            [self didChangeValueForKey:@"sensorValues"];
        });
    }
}


- (NSDictionary *)sensorValues
{
    return @{ @"ambientTemp": self.ambientTemp,
              @"relativeHumidity": self.relativeHumidity };
}

@end
