//
//  WLMonitorService.h
//  WeatherLog

//  请先setSpeed，设置当前风速，然后再读取体感温度
//
//  Created by 龙恒舟 on 15/9/15.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import "YMSCBService.h"

@interface WLMonitorService : YMSCBService

/**
 Dictionary containing the values measured by the sensor.
  */
@property (nonatomic, readonly) NSDictionary *sensorValues;

/// Pressure measurement
//@property (nonatomic, strong, readonly) NSNumber *pressure;

/// Ambient temperature measurement
//@property (nonatomic, strong, readonly) NSNumber *ambientTemp;

//@property (nonatomic, strong, readonly) NSNumber *relativeHumidity;

//@property (nonatomic, strong, readonly) NSNumber *UVI;

@property (nonatomic, assign, readonly) BOOL isCalibrating;

@property (nonatomic, assign, readonly) BOOL isCalibrated;

/**
 Request calibration of barometer.
 */
- (void)requestCalibration;

- (void)turnOn;

- (void)turnOff;

//  请先setSpeed，设置当前风速，然后再读取体感温度
- (void)setSpeed:(double) speed;

- (void) calcAllMonitorData:(NSData *)data pressure:(double *)pressure temp:(double *)temp humidity:(double *)humidity uvi:(double *)uvi;

//  请先setSpeed，设置当前风速，然后再读取体感温度
- (void) calcAllMonitorData:(NSData *)data pressure:(double *)pressure temp:(double *)temp humidity:(double *)humidity uvi:(double *)uvi sentemp:(double *)sentemp;

- (void) calcAllMonitorData1:(NSData *)data pressure:(double *)pressure temp:(double *)temp humidity:(double *)humidity uvi:(double *)uvi pressure1:(double *)pressure1 temp1:(double *)temp1 humidity1:(double *)humidity1 uvi1:(double *)uvi1;

//  请先setSpeed，设置当前风速，然后再读取体感温度
- (void) calcAllMonitorData1:(NSData *)data pressure:(double *)pressure temp:(double *)temp humidity:(double *)humidity uvi:(double *)uvi sentemp:(double *)sentemp pressure1:(double *)pressure1 temp1:(double *)temp1 humidity1:(double *)humidity1 uvi1:(double *)uvi1 sentemp1:(double *)sentemp1;

@end
