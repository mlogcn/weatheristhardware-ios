//
//  WLBaseServices.h
//  WeatherLog
//
//  Created by 龙恒舟 on 15/8/3.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import "YMSCBService.h"
#import "WeatherLog.h"

@interface WLBaseService : YMSCBService

/**
 Dictionary containing the values measured by the sensor.
 
 This is an abstract propery.
 */
@property (nonatomic, readonly) NSDictionary *sensorValues;


/**
 Turn on CoreBluetooth peripheral service.
 
 This method turns on the service by:
 
 *  writing to *config* characteristic to enable service.
 *  writing to *data* characteristic to enable notification.
 
 */
- (void)turnOn;

/**
 Turn off CoreBluetooth peripheral service.
 
 This method turns off the service by:
 
 *  writing to *config* characteristic to disable service.
 *  writing to *data* characteristic to disable notification.
 
 */
- (void)turnOff;

@end
