//
//  WLBarometerService.m
//  WeatherLog
//
//  Created by 龙恒舟 on 15/8/3.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import "WLBarometerService.h"
#import "YMSCBCharacteristic.h"
#import "WLWeatherLogUtils.h"

@interface WLBarometerService ()

@property (nonatomic, strong) NSNumber *pressure;
@property (nonatomic, strong) NSNumber *ambientTemp;

/// Calibration point
@property (nonatomic, assign) int16_t c1;
/// Calibration point
@property (nonatomic, assign) int16_t c2;
/// Calibration point
@property (nonatomic, assign) int16_t c3;
/// Calibration point
@property (nonatomic, assign) uint16_t c4;
/// Calibration point
@property (nonatomic, assign) uint16_t c5;
/// Calibration point
@property (nonatomic, assign) uint16_t c6;
/// Calibration point
@property (nonatomic, assign) int16_t c7;
/// Calibration point
@property (nonatomic, assign) int16_t c8;

@property (nonatomic, assign) int16_t c9;

@property (nonatomic, assign) int16_t c10;

@property (nonatomic, assign) int16_t c11;

@end

@implementation WLBarometerService


//double calcBarTmp(int16_t t_r, uint16_t c1, uint16_t c2) {
//    int32_t t_a;
//    
//    
//    t_a = (((((int32_t)c1 * t_r)/0x100) +
//            ((int32_t)c2 * 0x40))*100
//           ) / 0x10000;
//    
//    
//    return (double)t_a/100.0;
//}

//double calcBarTmp1(int16_t t_r, uint16_t c5, uint16_t c6, int16_t c10, int16_t c11) {
//    int32_t t_a;
//    
//    t_a = 1.0;
//    
//    return (double)t_a/100.0;
//}

//double calcBarPress(int16_t t_r,
//                    uint16_t p_r,   //为什么是无符号
//                    uint16_t c3,
//                    uint16_t c4,
//                    int16_t c5,
//                    int16_t c6,
//                    int16_t c7,
//                    int16_t c8) {
//    
//    int32_t p_a, S, O;
//    
//    //calculate pressure
//    S=c3+(((int32_t)c4*t_r)/0x20000)+(((((int32_t)c5*t_r)/0x8000)*t_r)/0x80000);
//    O=c6*0x4000+(((int32_t)c7*t_r)/8)+(((((int32_t)c8*t_r)/0x8000)*t_r)/16);
//    p_a=(S*p_r+O)/0x4000;
//    
//    // Unit: Pascal (Pa)
//    return (double)p_a;
//}

//double calcBarPress1(uint16_t t_r,
//                     uint16_t p_r,
//                     int16_t c1,
//                     int16_t c2,
//                     int16_t c3,
//                     uint16_t c4,
//                     uint16_t c5,
//                     uint16_t c6,
//                     int16_t c7,
//                     int16_t c8,
//                     int16_t c9,
//                     int16_t c10,
//                     int16_t c11,
//                     double *temp) {
//    
//    NSLog(@"The p_r is %d .", p_r);
//    NSLog(@"The t_r is %d .", t_r);
//    
//    double X1, X2, B5, t_a, p_a;
//    
//    X1 = (t_r - (int32_t)c6 ) * (int32_t)c5 / (double)(0x8000);
//    X2 = ((int32_t)c10 * 0x800) / (double)(X1 + (int32_t)c11);
//    B5 = X1 + X2;
//    t_a = (B5 + 8) / (double)0x10;
//    *temp = (double)t_a / 10.0;
//    
//    double B6, X3, B3;
//    uint32_t B4, B7;
//    B6 = B5 - 4000;
//    X1 = ((int32_t)c8 * (B6 * B6 / (double)0x1000)) / (double)0x800;
//    X2 = (int32_t)c2 * B6 / (double)0x800;
//    X3 = X1 + X2;
//    B3 = (((int32_t)c1 * 4 + X3)+2)/(double)4;
//    X1 = (int32_t)c3 * B6 / (double)0x2000;
//    X2 = ((int32_t)c7* (B6 * B6 / (double)0x1000)) / (double)0x10000;
//    X3 = ((X1 + X2) + 2)/(double)4;
//    B4 = (int32_t)c4 * ((uint32_t)(X3 + 32768)/(double)0x8000);
//    B7 = ((uint32_t)p_r - B3) * 50000;
//    if (B7 < 0x80000000) {
//        p_a = (B7 * 2 / (double)B4);
//    } else {
//        p_a = (B7 / (double)B4) * 2;
//    }
//    X1 = (p_a / (double)0x100)*(p_a / (double)0x100);
//    X1 = (X1 * 3038) / (double)0x10000;
//    X2 = (-7357 * p_a) / (double)0x10000;
//    p_a = p_a + (X1 + X2 + 3791) / (double)16;
//    
//    // Unit: Pascal (Pa)
//    return (double)p_a;
//}


- (instancetype)initWithName:(NSString *)oName
                      parent:(YMSCBPeripheral *)pObj
                      baseHi:(int64_t)hi
                      baseLo:(int64_t)lo
               serviceOffset:(int)serviceOffset {
    
    self = [super initWithName:oName
                        parent:pObj
                        baseHi:hi
                        baseLo:lo
                 serviceOffset:serviceOffset];
    
    if (self) {
        [self addCharacteristic:@"data" withOffset:kWeatherLog_BAROMETER_DATA];
        [self addCharacteristic:@"config" withOffset:kWeatherLog_BAROMETER_CONFIG];
        [self addCharacteristic:@"calibration" withOffset:kWeatherLog_BAROMETER_CALIBRATION];
        [self addCharacteristic:@"calibration1" withOffset:kWeatherLog_BAROMETER_CALIBRATION1];
        _isCalibrating = NO;
    }
    return self;
}

- (void)notifyCharacteristicHandler:(YMSCBCharacteristic *)yc error:(NSError *)error {
    
    if (error) {
        return;
    }
    
    if ([yc.name isEqualToString:@"data"]) {
        
        if (self.isCalibrated == NO) {
            return;
        }
        
        NSData *data = yc.cbCharacteristic.value;
        
        char val[data.length];
        [data getBytes:&val length:data.length];
//        NSLog(@"The data.length is %lu .", (unsigned long)data.length);
//        NSLog(@"The v0 is %d .", val[0]);
//        NSLog(@"The v1 is %d .", val[1]);
//        NSLog(@"The v2 is %d .", val[2]);
//        NSLog(@"The v3 is %d .", val[3]);
        
        int16_t v0 = val[0];
        int16_t v1 = val[1];
        int16_t v2 = val[2];
        int16_t v3 = val[3];
        
//        NSLog(@"The v0 is %d .", v0);
//        NSLog(@"The v1 is %d .", v1);
//        NSLog(@"The v2 is %d .", v2);
//        NSLog(@"The v3 is %d .", v3);
        
        uint16_t p_r = ((v3 & 0xff)| ((v2 << 8) & 0xff00));
        uint16_t t_r = ((v1 & 0xff)| ((v0 << 8) & 0xff00));
//        NSLog(@"The p_r is %d .", p_r);
//        NSLog(@"The t_r is %d .", t_r);
        
        
        //t_r = (int16_t)27898;
        //p_r = (int16_t)23843;
        __weak WLBarometerService *this = self;
        
        //        this.c1 = 408;
        //        this.c2 = -72;
        //        this.c3 = -14383;
        //        this.c4 = 32741;
        //        this.c5 = 32757;
        //        this.c6 = 23153;
        //        this.c7 = 6190;
        //        this.c8 = 4;
        //        this.c9 = -32768;
        //        this.c10 = -8711;
        //        this.c11 = 2868;
        
        
        _YMS_PERFORM_ON_MAIN_THREAD(^{
            
            double temp = 0.0;
            double pre = 0.0;
            pre = (calcBarPress(t_r, p_r, _c1, _c2, _c3, _c4, _c5, _c6, _c7, _c8, _c9, _c10, _c11, &temp));
//            NSLog(@"The temperature is %f .", temp);
//            NSLog(@"The pressure is %f .", pre);
            
            [self willChangeValueForKey:@"sensorValues"];
            //            this.pressure = @(calcBarPress(t_r, p_r, _c3, _c4, _c5, _c6, _c7, _c8));
            //            this.ambientTemp = @(calcBarTmp(t_r, _c1, _c2));
            this.pressure = @(pre);
            this.ambientTemp = @(temp);
            [self didChangeValueForKey:@"sensorValues"];
        });
    }
}

- (void)requestCalibration {
    if (self.isCalibrating == NO) {
        
        __weak WLBarometerService *this = self;
        
        YMSCBCharacteristic *configCt = self.characteristicDict[@"config"];
        [configCt writeByte:0x2 withBlock:^(NSError *error) {
            if (error) {
                NSLog(@"ERROR: write request to barometer config to start calibration failed.");
                return;
            }
            
            YMSCBCharacteristic *calibrationCt = this.characteristicDict[@"calibration"];
            [calibrationCt readValueWithBlock:^(NSData *data, NSError *error) {
                if (error) {
                    NSLog(@"ERROR: read request to barometer calibration failed.");
                    return;
                }
                
                YMSCBCharacteristic *calibrationCt1 = this.characteristicDict[@"calibration1"];
                
                [calibrationCt1 readValueWithBlock:^(NSData *data, NSError *error) {
                    if (error) {
                        NSLog(@"ERROR: read request to barometer calibration1 failed.");
                        return;
                    }
                    
                    this.isCalibrating = NO;
                    char val[data.length];
                    [data getBytes:&val length:data.length];
                    
                    int i = 0;
                    while (i < data.length) {
                        int16_t lo = val[i+1];
                        int16_t hi = val[i];
                        int16_t cx = ((lo & 0xff)| ((hi << 8) & 0xff00));
                        int index = i/2 + 1;
                        
                        if (index == 1) {
                            self.c9 = cx;
//                            NSLog(@"c9 is %d ", self.c9);
                        }
                        else if (index == 2) {
                            this.c10 = cx;
//                            NSLog(@"c10 is %d ", self.c10);
                        }
                        else if (index == 3) {
                            this.c11 = cx;
//                            NSLog(@"c11 is %d ", self.c11);
                        }
                        
                        i = i + 2;
                    }
                    
                }];
                
                this.isCalibrating = NO;
                char val[data.length];
                [data getBytes:&val length:data.length];
                
                int i = 0;
                while (i < data.length) {
                    int16_t hi = val[i];
                    int16_t lo = val[i+1];
                    int16_t cx = ((lo & 0xff)| ((hi << 8) & 0xff00));
                    uint16_t cx1 = ((lo & 0xff)| ((hi << 8) & 0xff00));
                    //NSLog(@"cx is %d ", cx);
                    //NSLog(@"cx1 is %u ", cx1);
                    int index = i/2 + 1;
                    
                    if (index == 1) {
                        self.c1 = cx;
//                        NSLog(@"c1 is %d ", self.c1);
                    }
                    else if (index == 2) {
                        this.c2 = cx;
//                        NSLog(@"c2 is %d ", self.c2);
                    }
                    else if (index == 3) {
                        this.c3 = cx;
//                        NSLog(@"c3 is %d ", self.c3);
                    }
                    else if (index == 4) {
                        this.c4 = cx1;
//                        NSLog(@"c4 is %u ", self.c4);
                    }
                    else if (index == 5) {
                        this.c5 = cx1;
//                        NSLog(@"c5 is %u ", self.c5);
                    }
                    else if (index == 6) {
                        this.c6 = cx1;
//                        NSLog(@"c6 is %u ", self.c6);
                    }
                    else if (index == 7) {
                        this.c7 = cx;
//                        NSLog(@"c7 is %d ", self.c7);
                    }
                    else if (index == 8) {
                        this.c8 = cx;
//                        NSLog(@"c8 is %d ", self.c8);
                    }
                    
                    i = i + 2;
                }
                
                this.isCalibrating = YES;
                this.isCalibrated = YES;
                
            }];
        }];
        
    }
}

- (NSDictionary *)sensorValues
{
    return @{ @"pressure": self.pressure,
              @"ambientTemp": self.ambientTemp };
}

@end
