//
//  WLHumidityService.h
//  WeatherLog
//
//  Created by 龙恒舟 on 15/8/3.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import "WLBaseService.h"

@interface WLHumidityService : WLBaseService

@property (nonatomic, strong, readonly) NSDictionary *sensorValues;

/// Ambient temperature
@property (nonatomic, strong, readonly) NSNumber *ambientTemp;

/// Relative humidity
@property (nonatomic, strong, readonly) NSNumber *relativeHumidity;

@end
