//
//  WLMonitorService.m
//  WeatherLog
//
//  Created by 龙恒舟 on 15/9/15.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import "WLMonitorService.h"
#import "YMSCBCharacteristic.h"
#import "YMSCBUtils.h"
#import "WeatherLog.h"
#import "WLWeatherLogUtils.h"

@interface WLMonitorService ()

@property (nonatomic, strong) NSNumber *pressure;
@property (nonatomic, strong) NSNumber *ambientTemp;
@property (nonatomic, strong) NSNumber *currentSpeed;   //当前风速，来自预报值
@property (nonatomic, strong) NSNumber *comfortLevel;    //温湿舒适度

@property (nonatomic, strong) NSNumber *UVI;
@property (nonatomic, strong) NSNumber *relativeHumidity;
@property (nonatomic, strong) NSNumber *sendibleTemp;  //体感温度

/// Calibration point
@property (nonatomic, assign) int16_t c1;
/// Calibration point
@property (nonatomic, assign) int16_t c2;
/// Calibration point
@property (nonatomic, assign) int16_t c3;
/// Calibration point
@property (nonatomic, assign) uint16_t c4;
/// Calibration point
@property (nonatomic, assign) uint16_t c5;
/// Calibration point
@property (nonatomic, assign) uint16_t c6;
/// Calibration point
@property (nonatomic, assign) int16_t c7;
/// Calibration point
@property (nonatomic, assign) int16_t c8;

@property (nonatomic, assign) int16_t c9;

@property (nonatomic, assign) int16_t c10;

@property (nonatomic, assign) int16_t c11;

@property (nonatomic, assign) BOOL isCalibrating;

@property (nonatomic, assign) BOOL isCalibrated;

@end

@implementation WLMonitorService

- (void) calcAllMonitorData:(NSData *)data pressure:(double *)pressure temp:(double *)temp humidity:(double *)humidity uvi:(double *)uvi {

    char val[data.length];
    [data getBytes:&val length:data.length];
    
    int16_t v0 = val[0];
    int16_t v1 = val[1];
    int16_t v2 = val[2];
    int16_t v3 = val[3];
    int16_t v4 = val[4];
    int16_t v5 = val[5];
    int16_t v6 = val[6];
    int16_t v7 = val[7];
    
    uint16_t p_r = yms_u16_build(v3, v2); //((v3 & 0xff)| ((v2 << 8) & 0xff00));
    uint16_t t_r = yms_u16_build(v1, v0); //((v1 & 0xff)| ((v0 << 8) & 0xff00));
    
    *pressure = (calcBarPress(t_r, p_r, _c1, _c2, _c3, _c4, _c5, _c6, _c7, _c8, _c9, _c10, _c11, temp));
    
    uint16_t rawH = yms_u16_build(v4, v5);
    *humidity = calcHumRel(rawH);
    
    uint16_t rawUVI = yms_u16_build(v7, v6);
    *uvi = rawUVI / 100.0;
}

- (void) calcAllMonitorData:(NSData *)data pressure:(double *)pressure temp:(double *)temp humidity:(double *)humidity uvi:(double *)uvi sentemp:(double *)sentemp {
    
    char val[data.length];
    [data getBytes:&val length:data.length];
    
    int16_t v0 = val[0];
    int16_t v1 = val[1];
    int16_t v2 = val[2];
    int16_t v3 = val[3];
    int16_t v4 = val[4];
    int16_t v5 = val[5];
    int16_t v6 = val[6];
    int16_t v7 = val[7];
    
    uint16_t p_r = yms_u16_build(v3, v2); //((v3 & 0xff)| ((v2 << 8) & 0xff00));
    uint16_t t_r = yms_u16_build(v1, v0); //((v1 & 0xff)| ((v0 << 8) & 0xff00));
    
    *pressure = (calcBarPress(t_r, p_r, _c1, _c2, _c3, _c4, _c5, _c6, _c7, _c8, _c9, _c10, _c11, temp));
    
    uint16_t rawH = yms_u16_build(v4, v5);
    *humidity = calcHumRel(rawH);
    
    uint16_t rawUVI = yms_u16_build(v7, v6);
    *uvi = rawUVI / 100.0;
    
    double speed = _currentSpeed.doubleValue;
    *sentemp = calcSenTemp(*temp, *humidity, speed);
}

- (void) calcAllMonitorData1:(NSData *)data pressure:(double *)pressure temp:(double *)temp humidity:(double *)humidity uvi:(double *)uvi pressure1:(double *)pressure1 temp1:(double *)temp1 humidity1:(double *)humidity1 uvi1:(double *)uvi1 {
    
    char val[data.length];
    [data getBytes:&val length:data.length];
    
    int16_t v0 = val[2];
    int16_t v1 = val[3];
    int16_t v2 = val[4];
    int16_t v3 = val[5];
    int16_t v4 = val[6];
    int16_t v5 = val[7];
    int16_t v6 = val[8];
    int16_t v7 = val[9];
    
    uint16_t p_r = yms_u16_build(v3, v2); //((v3 & 0xff)| ((v2 << 8) & 0xff00));
    uint16_t t_r = yms_u16_build(v1, v0); //((v1 & 0xff)| ((v0 << 8) & 0xff00));
    
    *pressure = (calcBarPress(t_r, p_r, _c1, _c2, _c3, _c4, _c5, _c6, _c7, _c8, _c9, _c10, _c11, temp));
    
    uint16_t rawH = yms_u16_build(v4, v5);
    *humidity = calcHumRel(rawH);
    
    uint16_t rawUVI = yms_u16_build(v7, v6);
    *uvi = rawUVI / 100.0;
    
    
    v0 = val[10];
    v1 = val[11];
    v2 = val[12];
    v3 = val[13];
    v4 = val[14];
    v5 = val[15];
    v6 = val[16];
    v7 = val[17];
    
    p_r = yms_u16_build(v3, v2); //((v3 & 0xff)| ((v2 << 8) & 0xff00));
    t_r = yms_u16_build(v1, v0); //((v1 & 0xff)| ((v0 << 8) & 0xff00));
    
    *pressure1 = (calcBarPress(t_r, p_r, _c1, _c2, _c3, _c4, _c5, _c6, _c7, _c8, _c9, _c10, _c11, temp1));
    
    rawH = yms_u16_build(v4, v5);
    *humidity1 = calcHumRel(rawH);
    
    rawUVI = yms_u16_build(v7, v6);
    *uvi1 = rawUVI / 100.0;
}

- (void) calcAllMonitorData1:(NSData *)data pressure:(double *)pressure temp:(double *)temp humidity:(double *)humidity uvi:(double *)uvi sentemp:(double *)sentemp pressure1:(double *)pressure1 temp1:(double *)temp1 humidity1:(double *)humidity1 uvi1:(double *)uvi1 sentemp1:(double *)sentemp1{
    
    char val[data.length];
    [data getBytes:&val length:data.length];
    
    int16_t v0 = val[2];
    int16_t v1 = val[3];
    int16_t v2 = val[4];
    int16_t v3 = val[5];
    int16_t v4 = val[6];
    int16_t v5 = val[7];
    int16_t v6 = val[8];
    int16_t v7 = val[9];
    
    uint16_t p_r = yms_u16_build(v3, v2); //((v3 & 0xff)| ((v2 << 8) & 0xff00));
    uint16_t t_r = yms_u16_build(v1, v0); //((v1 & 0xff)| ((v0 << 8) & 0xff00));
    
    *pressure = (calcBarPress(t_r, p_r, _c1, _c2, _c3, _c4, _c5, _c6, _c7, _c8, _c9, _c10, _c11, temp));
    
    uint16_t rawH = yms_u16_build(v4, v5);
    *humidity = calcHumRel(rawH);
    
    uint16_t rawUVI = yms_u16_build(v7, v6);
    *uvi = rawUVI / 100.0;
    
    double speed = _currentSpeed.doubleValue;
    *sentemp = calcSenTemp(*temp, *humidity, speed);
    
    v0 = val[10];
    v1 = val[11];
    v2 = val[12];
    v3 = val[13];
    v4 = val[14];
    v5 = val[15];
    v6 = val[16];
    v7 = val[17];
    
    p_r = yms_u16_build(v3, v2); //((v3 & 0xff)| ((v2 << 8) & 0xff00));
    t_r = yms_u16_build(v1, v0); //((v1 & 0xff)| ((v0 << 8) & 0xff00));
    
    *pressure1 = (calcBarPress(t_r, p_r, _c1, _c2, _c3, _c4, _c5, _c6, _c7, _c8, _c9, _c10, _c11, temp1));
    
    rawH = yms_u16_build(v4, v5);
    *humidity1 = calcHumRel(rawH);
    
    rawUVI = yms_u16_build(v7, v6);
    *uvi1 = rawUVI / 100.0;
    
    *sentemp1 = calcSenTemp(*temp1, *humidity1, speed);
}

- (instancetype)initWithName:(NSString *)oName
                      parent:(YMSCBPeripheral *)pObj
                      baseHi:(int64_t)hi
                      baseLo:(int64_t)lo
               serviceOffset:(int)serviceOffset {
    
    self = [super initWithName:oName
                        parent:pObj
                        baseHi:hi
                        baseLo:lo
                 serviceOffset:serviceOffset];
    
    if (self) {
        yms_u128_t pbase = self.base;
        self.uuid = [YMSCBUtils createCBUUID:&pbase withIntBLEOffset:serviceOffset];

        [self addCharacteristic:@"data" withOffset:kWeatherLog_BAROMETER_DATA];
        [self addCharacteristic:@"config" withOffset:kWeatherLog_BAROMETER_CONFIG];
        [self addCharacteristic:@"calibration" withOffset:kWeatherLog_BAROMETER_CALIBRATION];
        [self addCharacteristic:@"calibration1" withOffset:kWeatherLog_BAROMETER_CALIBRATION1];
        _isCalibrating = NO;
    }
    _currentSpeed = @(0.0);
    return self;
}

- (void)addCharacteristic:(NSString *)cname withOffset:(int)addrOffset {
    YMSCBCharacteristic *yc;
    
    yms_u128_t pbase = self.base;
    
    CBUUID *uuid = [YMSCBUtils createCBUUID:&pbase withIntBLEOffset:addrOffset];
    
    yc = [[YMSCBCharacteristic alloc] initWithName:cname
                                            parent:self.parent
                                              uuid:uuid
                                            offset:addrOffset];
    
    self.characteristicDict[cname] = yc;
}

- (void)turnOff {
    __weak WLMonitorService *this = self;
    
    YMSCBCharacteristic *configCt = self.characteristicDict[@"config"];
    [configCt writeByte:0x0 withBlock:^(NSError *error) {
        if (error) {
            NSLog(@"ERROR: %@", error);
            return;
        }
        
        NSLog(@"TURNED OFF: %@", this.name);
    }];
    
    YMSCBCharacteristic *dataCt = self.characteristicDict[@"data"];
    [dataCt setNotifyValue:NO withBlock:^(NSError *error) {
        NSLog(@"Data notification for %@ off", this.name);
        
    }];
    
    _YMS_PERFORM_ON_MAIN_THREAD(^{
        this.isOn = NO;
    });
}

- (void)turnOn {
    __weak WLMonitorService *this = self;
    
    YMSCBCharacteristic *configCt = self.characteristicDict[@"config"];
    [configCt writeByte:0x1 withBlock:^(NSError *error) {
        if (error) {
            NSLog(@"ERROR: %@", error);
            return;
        }
        
        NSLog(@"TURNED ON: %@", this.name);
    }];
    
    YMSCBCharacteristic *dataCt = self.characteristicDict[@"data"];
    [dataCt setNotifyValue:YES withBlock:^(NSError *error) {
        NSLog(@"Data notification for %@ on", this.name);
        
    }];
    
    _YMS_PERFORM_ON_MAIN_THREAD(^{
        this.isOn = YES;
    });
}

- (void)notifyCharacteristicHandler:(YMSCBCharacteristic *)yc error:(NSError *)error {
    
    if (error) {
        return;
    }
    NSLog(@"Received the monitor notification!");
    if ([yc.name isEqualToString:@"data"]) {
        
        if (self.isCalibrated == NO) {
            return;
        }
        
        NSData *data = yc.cbCharacteristic.value;
        double pressure = 0.0;
        double temp = 0.0;
        double humidity = 0.0;
        double uvi = 0.0;
        double sentemp = 0.0;
        
        [self calcAllMonitorData:data pressure:&pressure temp:&temp humidity:&humidity uvi:&uvi sentemp:&sentemp];
        
        double comfort = 0.0;
        double tempF = temp * 1.8 + 32.0;
        comfort = tempF - (0.55 - 0.55 * humidity * 0.01) * (tempF - 58.0);

        
//        char val[data.length];
//        [data getBytes:&val length:data.length];
//        
//        int16_t v0 = val[0];
//        int16_t v1 = val[1];
//        int16_t v2 = val[2];
//        int16_t v3 = val[3];
//        
//        uint16_t p_r = ((v3 & 0xff)| ((v2 << 8) & 0xff00));
//        uint16_t t_r = ((v1 & 0xff)| ((v0 << 8) & 0xff00));
        
        __weak WLMonitorService *this = self;
        
        _YMS_PERFORM_ON_MAIN_THREAD(^{
            
//            double temp = 0.0;
//            double pre = 0.0;
//            pre = (calcBarPress(t_r, p_r, _c1, _c2, _c3, _c4, _c5, _c6, _c7, _c8, _c9, _c10, _c11, &temp));
            //            NSLog(@"The temperature is %f .", temp);
            //            NSLog(@"The pressure is %f .", pre);
            
            [self willChangeValueForKey:@"sensorValues"];
            //            this.pressure = @(calcBarPress(t_r, p_r, _c3, _c4, _c5, _c6, _c7, _c8));
            //            this.ambientTemp = @(calcBarTmp(t_r, _c1, _c2));
            this.pressure = @(pressure);
            this.ambientTemp = @(temp);
            this.UVI = @(uvi);
            this.relativeHumidity = @(humidity);
            this.sendibleTemp = @(sentemp);
            this.comfortLevel = @(comfort);
            
            [self didChangeValueForKey:@"sensorValues"];
            NSLog(@"Finished calc the monitor data!");
        });
    }
}

- (void)requestCalibration {
    if (self.isCalibrating == NO) {
        
        __weak WLMonitorService *this = self;
        NSLog(@"Starting calibrate the barometer!");
        YMSCBCharacteristic *configCt = self.characteristicDict[@"config"];
        [configCt writeByte:0x2 withBlock:^(NSError *error) {
            if (error) {
                NSLog(@"ERROR: write request to barometer config to start calibration failed.");
                return;
            }
            
            YMSCBCharacteristic *calibrationCt = this.characteristicDict[@"calibration"];
            [calibrationCt readValueWithBlock:^(NSData *data, NSError *error) {
                if (error) {
                    NSLog(@"ERROR: read request to barometer calibration failed.");
                    return;
                }
                
                YMSCBCharacteristic *calibrationCt1 = this.characteristicDict[@"calibration1"];
                
                [calibrationCt1 readValueWithBlock:^(NSData *data, NSError *error) {
                    if (error) {
                        NSLog(@"ERROR: read request to barometer calibration1 failed.");
                        return;
                    }
                    
                    this.isCalibrating = NO;
                    char val[data.length];
                    [data getBytes:&val length:data.length];
                    
                    int i = 0;
                    while (i < data.length) {
                        int16_t lo = val[i+1];
                        int16_t hi = val[i];
                        int16_t cx = ((lo & 0xff)| ((hi << 8) & 0xff00));
                        int index = i/2 + 1;
                        
                        if (index == 1) {
                            self.c9 = cx;
                            //                            NSLog(@"c9 is %d ", self.c9);
                        }
                        else if (index == 2) {
                            this.c10 = cx;
                            //                            NSLog(@"c10 is %d ", self.c10);
                        }
                        else if (index == 3) {
                            this.c11 = cx;
                            //                            NSLog(@"c11 is %d ", self.c11);
                        }
                        i = i + 2;
                    }
                    
                }];
                
                this.isCalibrating = NO;
                char val[data.length];
                [data getBytes:&val length:data.length];
                
                int i = 0;
                while (i < data.length) {
                    int16_t hi = val[i];
                    int16_t lo = val[i+1];
                    int16_t cx = ((lo & 0xff)| ((hi << 8) & 0xff00));
                    uint16_t cx1 = ((lo & 0xff)| ((hi << 8) & 0xff00));
                    //NSLog(@"cx is %d ", cx);
                    //NSLog(@"cx1 is %u ", cx1);
                    int index = i/2 + 1;
                    
                    if (index == 1) {
                        self.c1 = cx;
                        //                        NSLog(@"c1 is %d ", self.c1);
                    }
                    else if (index == 2) {
                        this.c2 = cx;
                        //                        NSLog(@"c2 is %d ", self.c2);
                    }
                    else if (index == 3) {
                        this.c3 = cx;
                        //                        NSLog(@"c3 is %d ", self.c3);
                    }
                    else if (index == 4) {
                        this.c4 = cx1;
                        //                        NSLog(@"c4 is %u ", self.c4);
                    }
                    else if (index == 5) {
                        this.c5 = cx1;
                        //                        NSLog(@"c5 is %u ", self.c5);
                    }
                    else if (index == 6) {
                        this.c6 = cx1;
                        //                        NSLog(@"c6 is %u ", self.c6);
                    }
                    else if (index == 7) {
                        this.c7 = cx;
                        //                        NSLog(@"c7 is %d ", self.c7);
                    }
                    else if (index == 8) {
                        this.c8 = cx;
                        //                        NSLog(@"c8 is %d ", self.c8);
                    }
                    
                    i = i + 2;
                }
                
                this.isCalibrating = YES;
                this.isCalibrated = YES;
                NSLog(@"Finished calibrate the barometer");
            }];
        }];
    }
}

- (void)setSpeed:(double) speed {
    _currentSpeed = @(speed);
}

- (NSDictionary *)sensorValues
{
    return @{ @"pressure": self.pressure,
              @"ambientTemp": self.ambientTemp,
              @"sendibleTemp": self.sendibleTemp,
              @"relativeHumidity": self.relativeHumidity,
              @"comfortLevel": self.comfortLevel,
              @"UVI": self.UVI};
}

@end
