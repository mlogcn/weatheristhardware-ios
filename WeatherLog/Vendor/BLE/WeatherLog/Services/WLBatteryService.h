//
//  WLBatteryLevelService.h
//  WeatherLog
//
//  Created by 龙恒舟 on 15/8/25.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import "WLBaseService.h"

@interface WLBatteryService : YMSCBService


/// Ultra Violet Index
@property (nonatomic, strong, readonly) NSNumber *batteryLevel;

/**
 Read the battery level.
 
 */
- (void)readBatteryInfo;

@end
