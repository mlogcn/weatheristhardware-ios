//
//  WLCentralManager.m
//  WeatherLog
//
//  Created by 龙恒舟 on 15/8/3.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import "WLCentralManager.h"
#import "WLWeatherLog.h"
#import "YMSCBStoredPeripherals.h"
#import "WeatherLog.h"


#define CALLBACK_EXAMPLE 1
static WLCentralManager *sharedCentralManager;

@implementation WLCentralManager


+ (WLCentralManager *)initSharedServiceWithDelegate:(id)delegate {
    if (sharedCentralManager == nil) {
        dispatch_queue_t queue = dispatch_queue_create("com.mlogcn.www", 0);    //串行队列
        
        //初始化knownPeripheralNames数组，不是_ymsPeripherals
        NSArray *nameList = @[@"WeatherLog", @"Mlog WeatherLog",@"TI BLE Sensor Tag",@"SensorTag", @"Thermometer Sensor"];
        sharedCentralManager = [[super allocWithZone:NULL] initWithKnownPeripheralNames:nameList
                                                                                  queue:queue
                                                                   useStoredPeripherals:YES
                                                                               delegate:delegate];
    }
    return sharedCentralManager;
    
}


+ (WLCentralManager *)sharedService {
    if (sharedCentralManager == nil) {
        NSLog(@"ERROR: must call initSharedServiceWithDelegate: first.");
    }
    return sharedCentralManager;
}

- (void)startScan {
    /*
     Setting CBCentralManagerScanOptionAllowDuplicatesKey to YES will allow for repeated updates of the RSSI via advertising.
     */
    
    NSDictionary *options = @{ CBCentralManagerScanOptionAllowDuplicatesKey: @YES };

#ifdef CALLBACK_EXAMPLE
    __weak WLCentralManager *this = self;
    [self scanForPeripheralsWithServices:nil
                                 options:options
                               withBlock:^(CBPeripheral *peripheral, NSDictionary *advertisementData, NSNumber *RSSI, NSError *error) {
                                   if (error) {
                                       NSLog(@"Something bad happened with scanForPeripheralWithServices:options:withBlock:");
                                       return;
                                   }
                                   
                                   NSLog(@"0000000000DISCOVERED: %@, %@, %@ db", peripheral.identifier, peripheral.name, RSSI);
                                   [this handleFoundPeripheral:peripheral];
                               }];
    
#else
    [self scanForPeripheralsWithServices:nil options:options];
#endif
    
}

- (void)handleFoundPeripheral:(CBPeripheral *)peripheral {
    YMSCBPeripheral *yp = [self findPeripheral:peripheral];
    
    if (yp == nil) {
        BOOL isUnknownPeripheral = YES;
        for (NSString *pname in self.knownPeripheralNames) {
            if ([pname isEqualToString:peripheral.name]) {
                WLWeatherLog *weatherLog = [[WLWeatherLog alloc] initWithPeripheral:peripheral central:self baseHi:kWeatherLog_BASE_ADDRESS_HI baseLo:kWeatherLog_BASE_ADDRESS_LO];
                
                [self addPeripheral:weatherLog];
                isUnknownPeripheral = NO;
                break;
                
            }
        }
        //除了WeatherLog和Mlogcn WeatherLog，都不添加
        /*
        if (isUnknownPeripheral) {
            //TODO: Handle unknown peripheral
            yp = [[YMSCBPeripheral alloc] initWithPeripheral:peripheral central:self baseHi:0 baseLo:0];
            [self addPeripheral:yp];
        }
         */
    }
    
}


- (void)managerPoweredOnHandler {
  
    if (self.useStoredPeripherals) {
#if TARGET_OS_IPHONE
        NSArray *identifiers = [YMSCBStoredPeripherals genIdentifiers];
//        [self retrievePeripheralsWithIdentifiers:identifiers];
#endif
    }
}


@end
