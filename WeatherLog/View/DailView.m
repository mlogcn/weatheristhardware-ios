//
//  DailView.m
//  WeatherLog
//
//  Created by ink on 15/8/31.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import "DailView.h"
#define Iphone6p 736
#define Iphone6 667
#define Iphone5 568
#define Iphone4 480
#define ScreenHeigth [UIScreen mainScreen].bounds.size.height
#define ScreenWidth [UIScreen mainScreen].bounds.size.width
#define COLOR(a,b,c,d) [UIColor colorWithRed:a/255.0 green:b/255.0 blue:c/255.0 alpha:d]

@implementation DailView
- (instancetype)init
{
    self = [super init];
    if (self) {
        switch ((NSInteger)ScreenHeigth) {
            case Iphone6:
            case Iphone6p:
                contentSize = CGSizeMake(162, 162);
                break;
            case Iphone5:
            case Iphone4:{
                contentSize = CGSizeMake(128, 128);
                break;
            }
            default:
                break;
        }

        [self addContent];
    }
    return self;
}

//- (instancetype)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self) {
//      
//        [self addContent];
//    }
//    return self;
//}
- (void)addContent{
    
//    UIButton *button2=[[UIButton alloc]init];
//    [button2 setTitle:@"重新扫描" forState:UIControlStateNormal];
//    button2.translatesAutoresizingMaskIntoConstraints=NO;
//    [button2 setBackgroundColor:[UIColor blueColor]];
//    [self addSubview:button2];
//    NSArray * constraints5 = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-40-[button2(==80)]"
//                                                                     options:0
//                                                                     metrics:nil
//                                                                       views:NSDictionaryOfVariableBindings(button2)];
//    NSArray * constraints6 = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[button2(==30)]-150-|"
//                                                                     options:0
//                                                                     metrics:nil
//                                                                       views:NSDictionaryOfVariableBindings(button2)];
//    [self addConstraints:constraints5];
//    [self addConstraints:constraints6];
//    button2.tag = 3;
//    [button2 addTarget:self action:@selector(buttonTouch:) forControlEvents:UIControlEventTouchUpInside];

    [self addThermometer];
    [self addHumidty];
    [self addAirPressure];
    [self addUV];
    
//    UIButton *button=[[UIButton alloc]init];
//    [button setTitle:@"扫描添加设备" forState:UIControlStateNormal];
//    button.translatesAutoresizingMaskIntoConstraints=NO;
//    [button setBackgroundColor:[UIColor cyanColor]];
//    [self addSubview:button];
//    NSArray * constraints1 = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[button]-|"
//                                                                     options:0
//                                                                     metrics:nil
//                                                                       views:NSDictionaryOfVariableBindings(button)];
//    NSArray * constraints2 = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[button(==30)]-80-|"
//                                                                     options:0
//                                                                     metrics:nil
//                                                                       views:NSDictionaryOfVariableBindings(button)];
//    [self addConstraints:constraints1];
//    [self addConstraints:constraints2];
//    button.tag = 1;
//    [button addTarget:self action:@selector(buttonTouch:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *button1=[[UIButton alloc]init];
    [button1 setTitle:@"设备管理" forState:UIControlStateNormal];
    button1.translatesAutoresizingMaskIntoConstraints=NO;
    [button1 setBackgroundColor:COLOR(135, 206, 250, 0.3)];
    [self addSubview:button1];
    NSArray * constraints3 = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[button1(==80)]-40-|"
                                                                     options:0
                                                                     metrics:nil
                                                                       views:NSDictionaryOfVariableBindings(button1)];
    NSArray * constraints4 = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[button1(==30)]-50-|"
                                                                     options:0
                                                                     metrics:nil
                                                                       views:NSDictionaryOfVariableBindings(button1)];
    [self addConstraints:constraints3];
    [self addConstraints:constraints4];
    button1.tag = 2;
    [button1 addTarget:self action:@selector(buttonTouch:) forControlEvents:UIControlEventTouchUpInside];
    
    
    notifiyLabel = [UILabel new];
    notifiyLabel.text = @"世界那么大，我想去看看";
    notifiyLabel.font = [UIFont boldSystemFontOfSize:15];
    notifiyLabel.textColor = [UIColor whiteColor];
    notifiyLabel.numberOfLines = 0;
    [self addSubview:notifiyLabel];
    [notifiyLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).offset(20);
        make.top.equalTo(self.mas_bottom).offset(-200);
        make.width.mas_greaterThanOrEqualTo(0);
        make.height.mas_greaterThanOrEqualTo(0);
        make.width.mas_lessThanOrEqualTo(ScreenWidth / 2 - 20);
    }];
    
    notifiyLableSecond = [UILabel new];
    notifiyLableSecond.text = @"身体这么好，哪都可以去";
    notifiyLableSecond.font = [UIFont boldSystemFontOfSize:15];
    notifiyLableSecond.textColor = [UIColor whiteColor];
    notifiyLableSecond.numberOfLines = 0;
    [self addSubview:notifiyLableSecond];
    [notifiyLableSecond mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-20);
        make.top.equalTo(self.mas_bottom).offset(-200);
        make.width.mas_greaterThanOrEqualTo(0);
        make.height.mas_greaterThanOrEqualTo(0);
        make.width.mas_lessThanOrEqualTo(ScreenWidth / 2 - 20);
    }];


}
/**
 *  添加温度计
 */
- (void)addThermometer{
    UIImage * backImage = [UIImage imageNamed:@"dial_temp.png"];

    _thermomter = [Thermomter new];
    [self addSubview:_thermomter];
    [_thermomter mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).with.offset(10);
        make.top.equalTo(self.mas_top).with.offset(70);
        make.size.mas_equalTo(contentSize);
    }];
    _thermomter.minValue = 0;
    _thermomter.maxValue = 100;
    
}
/**
 *  添加湿度计
 */
- (void)addHumidty{
    _humidty = [[CircleRing alloc] initWithFrame:CGRectMake(0, 0, 160, 160)];
    [self addSubview:_humidty];
    [_humidty mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).with.offset(-10);
        make.top.equalTo(self.mas_top).with.offset(70);
        make.size.mas_equalTo(contentSize);
        
    }];
    _humidty.minValue = 0;
    _humidty.maxValue = 100;
    
    
    
}
/**
 *  添加气压计
 */
- (void)addAirPressure{
    _airPressure = [AirPressure new];
    [self addSubview:_airPressure];
    [_airPressure mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left).with.offset(10);
        make.top.equalTo(self.mas_top).with.offset(300);
        make.size.mas_equalTo(contentSize);
        
    }];
    _airPressure.minValue = 0;
    _airPressure.maxValue = 100;
    
}
/**
 *  添加uv计
 */
- (void)addUV{
    _uvView = [UVView new];
    [self addSubview:_uvView];
    [_uvView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).with.offset(-10);
        make.top.equalTo(self.mas_top).with.offset(300);
        make.size.mas_equalTo(contentSize);
        
    }];
    _uvView.minValue = 0;
    _uvView.maxValue = 12;
    
}
- (void)buttonTouch:(UIButton *)button{
    [self.delegate buttonSelectWithIndex:button.tag];
}
- (void)changeValueWithDeviceTag:(NSInteger)tag AndValue:(CGFloat)number{
    switch (tag) {
        case 1:{
            [_airPressure moveToValue:number];
            _airPressure.pressureLabel.text = [NSString stringWithFormat:@"%.1f", number*(1040-980)/100 +980];
            break;
        }
        case 2:{
            [_uvView moveToValue:number];
            _uvView.uviLabel.text = [NSString stringWithFormat:@"%.1f", number];
            break;
        }
        case 3:{
            [_thermomter movehandleToValue:45 + 1.125 * number];
            _thermomter.tempLabel.text = [NSString stringWithFormat:@"%.1f°",number];
            break;
            
        }
            
        default:
            break;
    }
}
- (void)changeTempAndHum:(WLHumidityService *)service{
    [_thermomter movehandleToValue:45 + 1.125 * service.ambientTemp.floatValue];
    _thermomter.tempLabel.text = [NSString stringWithFormat:@"%.1f°",service.ambientTemp.floatValue];
    [_humidty movehandleToValue:service.relativeHumidity.floatValue];
    _humidty.humLabel.text = [NSString stringWithFormat:@"%ld%%",(long)service.relativeHumidity.floatValue];
    
}

- (void)changeTempAndHum:(NSNumber *)ambientTemp humidity:(NSNumber *)relativeHumidity {
    [_thermomter movehandleToValue:45 + 1.125 * ambientTemp.floatValue];
    _thermomter.tempLabel.text = [NSString stringWithFormat:@"%.1f°",ambientTemp.floatValue];
    [_humidty movehandleToValue:relativeHumidity.floatValue];
    _humidty.humLabel.text = [NSString stringWithFormat:@"%ld%%",(long)relativeHumidity.floatValue];
}
- (void)setNotifiyText:(NSString *)notifiyString{
    notifiyLabel.text = notifiyString;
}
- (void)setOtherNotifiyText:(NSString *)notifiyString{
    notifiyLableSecond.text = notifiyString;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
