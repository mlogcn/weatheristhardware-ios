//
//  DailView.h
//  WeatherLog
//
//  Created by ink on 15/8/31.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Thermomter.h"
#import "CircleRing.h"
#import "AirPressure.h"
#import "UVView.h"
#import "WLHumidityService.h"
#import <Masonry.h>
@protocol DailDelegate
- (void)buttonSelectWithIndex:(NSInteger)index;
@end

@interface DailView : UIView{
    Thermomter * _thermomter;
    CircleRing * _humidty;
    AirPressure * _airPressure;
    UVView * _uvView;
    CGSize contentSize;
    UILabel * notifiyLabel;
    UILabel * notifiyLableSecond;
}
@property (nonatomic, assign) id<DailDelegate>delegate;
- (void)changeValueWithDeviceTag:(NSInteger)tag AndValue:(CGFloat)number ;
- (void)changeTempAndHum:(WLHumidityService *)service;
- (void)changeTempAndHum:(NSNumber *)ambientTemp humidity:(NSNumber *)relativeHumidity;
- (void)setNotifiyText:(NSString *)notifiyString;
- (void)setOtherNotifiyText:(NSString *)notifiyString;
@end
