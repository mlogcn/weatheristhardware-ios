//
//  LineView.h
//  WeatherLog
//
//  Created by ink on 15/9/7.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ReactiveCocoa.h>
#import <Masonry.h>
#import "WeatherCurve.h"
@interface LineView : UIView{
    UIScrollView * _lineScrollView;
    WeatherCurve * _weatherCurve;
}

@property (nonatomic, strong) NSArray * weatherDataArray;

@end
