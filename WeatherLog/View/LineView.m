//
//  LineView.m
//  WeatherLog
//
//  Created by ink on 15/9/7.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import "LineView.h"
#define ScreenHeigth [UIScreen mainScreen].bounds.size.height
#define ScreenWidth [UIScreen mainScreen].bounds.size.width

@implementation LineView
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self addScrollView];
        [RACObserve(self, self.weatherDataArray) subscribeNext:^(id x) {
            _weatherCurve.lineArray = (NSArray *)x;
        }];
    }
    return self;
}

- (void)addScrollView{
    _lineScrollView = [UIScrollView new];
    _lineScrollView.showsHorizontalScrollIndicator = NO;
    [self addSubview:_lineScrollView];
    [_lineScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self);
        make.right.equalTo(self);
        make.bottom.equalTo(self);
        make.top.equalTo(self.mas_bottom).offset(-300);
    }];
    
    UIView * contentView = [UIView new];
    [_lineScrollView addSubview:contentView];
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(_lineScrollView);
        make.height.equalTo(_lineScrollView);
    }];

    _weatherCurve = [WeatherCurve new];
    [contentView addSubview:_weatherCurve];
    [_weatherCurve mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(contentView.mas_top);
        make.bottom.equalTo(contentView.mas_bottom);
        make.left.equalTo(contentView.mas_left).offset(ScreenWidth / 2);
        make.width.mas_equalTo(ScreenWidth * 7);
    }];
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_weatherCurve.mas_right).offset(ScreenWidth / 2);
    }];
    UIButton * changeButton = [UIButton new];
    [changeButton setBackgroundColor:[UIColor redColor]];
    [self addSubview:changeButton];
    [changeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right).offset(-40);
        make.top.equalTo(self.mas_top).offset(40);
        make.size.mas_equalTo(CGSizeMake(70, 30));
    }];
    [changeButton addTarget:self action:@selector(changeCurve) forControlEvents:UIControlEventTouchUpInside];

}
- (void)changeCurve{
    
    [UIView animateWithDuration:0.5 animations:^{
        _lineScrollView.alpha = 0;
    } completion:^(BOOL finished) {
        [_weatherCurve changeCureveToAnother];
        [UIView animateWithDuration:0.5 animations:^{
            _lineScrollView.alpha = 1.0;
        }];
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
