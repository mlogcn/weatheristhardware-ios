//
//  UVView.m
//  WeatherLog
//
//  Created by ink on 15/8/27.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import "UVView.h"

@implementation UVView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self addBackImageView];
        [self addPointer];
        
        self.uviLabel = [UILabel new];
        self.uviLabel.textColor = [UIColor whiteColor];
        self.uviLabel.textAlignment = NSTextAlignmentCenter;
        self.uviLabel.font = [UIFont boldSystemFontOfSize:16];
        [self addSubview:self.uviLabel];
        [self.uviLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.bottom.equalTo(self.mas_bottom).offset(-30);
            make.width.greaterThanOrEqualTo(@0);
            make.height.greaterThanOrEqualTo(@0);
        }];
    }
    return self;
}
- (void)addBackImageView{
    UIImage *backImage = [UIImage imageNamed:@"uv.png"];
    UIImageView * backImageView = [UIImageView new];
    [self addSubview:backImageView];
    backImageView.image = backImage;
    [backImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self).with.insets(UIEdgeInsetsZero);
        
    }];
}
- (void)addPointer{
    UIImage * pointerImage = [UIImage imageNamed:@"dial_wind_ico"];
    _pointerImageView = [UIImageView new];
    [self addSubview:_pointerImageView];
    _pointerImageView.image = pointerImage;
    [_pointerImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(pointerImage.size);
        make.center.mas_equalTo(self.center);
    }];
}

- (void)moveToValue:(CGFloat)value{
    if (value>self.maxValue) {
        value=self.maxValue;
    }else if (value<self.minValue){
        value= self.minValue;
    }
    CGFloat angle = M_PI * 2 * value / self.maxValue;
    _pointerImageView.layer.transform = CATransform3DMakeRotation(-M_PI + angle, 0, 0, 1);
    
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
