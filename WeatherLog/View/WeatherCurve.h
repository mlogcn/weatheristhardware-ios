//
//  WeatherCurve.h
//  
//
//  Created by ink on 15/9/11.
//
//

#import <UIKit/UIKit.h>
#import <ReactiveCocoa.h>
#import "HourModel.h"
#import <Masonry.h>
@interface WeatherCurve : UIView{
    NSInteger maxYpoint;
    NSInteger otherHeight;
    NSMutableArray * tempPointArray;
    NSMutableArray * otherPointArray;
    NSMutableArray * humPointArray;
    NSMutableArray * pressurePointArray;
    NSMutableArray * uvPointArray;

}
@property (nonatomic, strong) NSArray * lineArray;
- (void)changeCureveToAnother;

@end
