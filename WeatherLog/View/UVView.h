//
//  UVView.h
//  WeatherLog
//
//  Created by ink on 15/8/27.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Masonry.h>
@interface UVView : UIView{
    UIImageView * _pointerImageView;

}
@property (nonatomic) float maxValue;
@property (nonatomic) float minValue;
- (void)moveToValue:(CGFloat)value;

@property (nonatomic, strong) UILabel * uviLabel;

@end
