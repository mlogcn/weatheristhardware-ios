//
//  AppDelegate.h
//  WeatherLog
//
//  Created by 龙恒舟 on 15/8/3.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RootViewController.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

