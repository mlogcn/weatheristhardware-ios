//
//  NetUrl.h
//  WeatherLog
//
//  Created by ink on 15/9/8.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#ifndef WeatherLog_NetUrl_h
#define WeatherLog_NetUrl_h
#define BasicUrl @"http://api.weather.mlogcn.com:8000/"
//#define AllDayWeather(lon,lat) [NSString stringWithFormat:@"api/weather/v2/fc/24h/coor/%@/%@.json",lon,lat]
#define WeekDetailWeather(lon,lat) [NSString stringWithFormat:@"api/weather/v2/hourly/plot/coor/%@/%@.json",lon,lat]

#endif
