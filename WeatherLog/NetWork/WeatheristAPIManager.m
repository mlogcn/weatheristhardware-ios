//
//  WeatheristAPIManager.m
//  WeatherLog
//
//  Created by ink on 15/9/8.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import "WeatheristAPIManager.h"
#import "NetUrl.h"
@implementation WeatheristAPIManager

+ (instancetype)shareManager{
    static WeatheristAPIManager * instance;
    static dispatch_once_t  onceToken;
    dispatch_once(&onceToken, ^{
        instance = [self manager];
    });
    
    return instance;
    
}
- (RACSignal *)fetchWeatherData{
    return [[RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        INTULocationManager * locMag = [INTULocationManager sharedInstance];
        [locMag requestLocationWithDesiredAccuracy:INTULocationAccuracyBlock
                                           timeout:5
                              delayUntilAuthorized:YES
                                             block: ^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                                 [subscriber sendNext:currentLocation];
                                                 [subscriber sendCompleted];
                                                 
                                             }];
        return nil;

    }] map:^id(CLLocation * location) {
        NSLog(@"%f",location.coordinate.latitude);
        
        NSString * allDayUrlString = [BasicUrl stringByAppendingString:WeekDetailWeather([NSNumber numberWithFloat:location.coordinate.longitude], [NSNumber numberWithFloat:location.coordinate.latitude])];
        NSURLRequest * requelt = [NSURLRequest requestWithURL:[NSURL URLWithString:allDayUrlString]];
        NSURLResponse *response = nil;
        NSError *error = nil;
        NSData * data = [NSURLConnection sendSynchronousRequest:requelt returningResponse:&response error:&error];
        NSDictionary * responseDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
        NSMutableArray * array = [NSMutableArray array];
        
        NSArray * dataArray = [responseDic objectForKey:@"W_24h"];
        for (int i = 0; i < dataArray.count; i++) {
            HourModel * model = [[HourModel alloc] initWithDic:dataArray[i]];
            if (i > 0) {
                NSDictionary * otherDic = dataArray[i - 1];
                HourModel * judgeModel = [[HourModel alloc] initWithDic:otherDic];
                if (![[model.time substringToIndex:10] isEqualToString:[judgeModel.time substringToIndex:10  ]]) {
                    [array addObject:model];
                }
                
                
            }else{
                [array addObject:model];
            }
            
        }

        return array;
    }];
}
//- (RACSignal *)fetchEveryDayWeatherDataWithURL:(NSString *)urlString{
////    return [[self rac_GET:urlString parameters:nil] map:^id(NSDictionary *data) {
////        NSLog(@"%@",data);
////        return nil;
////    }];
//    return [[self rac_GET:urlString parameters:nil] map:^id(id value) {
//        return nil;
//    }];
//}


@end
