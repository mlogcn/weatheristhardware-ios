//
//  WeatheristAPIManager.h
//  WeatherLog
//
//  Created by ink on 15/9/8.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>
#import <RACSignal.h>
#import "AFHTTPRequestOperationManager+RACSupport.h"
#import "INTULocationRequest.h"
#import "INTULocationManager.h"
#import "HourModel.h"
@interface WeatheristAPIManager : AFHTTPRequestOperationManager

+ (instancetype)shareManager;
- (RACSignal *)fetchWeatherData;
@end
