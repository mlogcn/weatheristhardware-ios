//
//  WLFetchWeatherData.m
//  WeatherLog
//
//  Created by 龙恒舟 on 15/9/22.
//  Copyright © 2015年 龙恒舟. All rights reserved.
//

#import "WLHandleWeatherData.h"

@implementation WLHandleWeatherData


+ (NSArray *) GetTheWeatherDataByName:(NSString *) name Duration:(NSNumber *) duration Offset:(NSNumber *) offset {
    NSArray *result;
    if (duration.doubleValue == 1.0 && offset.doubleValue == 0.0) {
        if ([name isEqualToString:@"uv_index"]) {
            result = [self GetWeatherData_UVI_RealTime];
        }else if ([name isEqualToString:@"sen_temp"]) {
            result = [self GetWeatherData_SensibleTemp_RealTime];
        }else if ([name isEqualToString:@"relative_humidity"]) {
            result = [self GetWeatherData_RelativeHumidity_RealTime];
        }else if ([name isEqualToString:@"ambient_temp"]) {
            result = [self GetWeatherData_AmbientTemp:duration Offset:offset];
        }else if ([name isEqualToString:@"comfort_level"]) {
            result = [self GetWeatherData_ComfortLevel_RealTime];
        }else {
            result = [NSArray array];
        }

    }else{
        if ([name isEqualToString:@"uv_index"]) {
            result = [self GetWeatherData_UVI:duration Offset:offset];
        }else if ([name isEqualToString:@"sen_temp"]) {
            result = [self GetWeatherData_SensibleTemp:duration Offset:offset];
        }else if ([name isEqualToString:@"out_index"]) {
            result = [self GetWeatherData_OutdoorIndex:duration Offset:offset];
        }else if ([name isEqualToString:@"comfort_leve"]) {
            result = [self GetWeatherData_ComfortLevel:duration Offset:offset];
        }else if ([name isEqualToString:@"relative_humidity"]) {
            result = [self GetWeatherData_RelativeHumidity:duration Offset:offset];
        }else if ([name isEqualToString:@"ambient_temp"]) {
            result = [self GetWeatherData_AmbientTemp:duration Offset:offset];
        }else {
            result = [NSArray array];
        }
    }

//    NSMutableArray *tempResult = [NSMutableArray array];
//    [tempResult addObject:@(0.9)];
//    [tempResult addObject:@(0.5)];
//    [tempResult addObject:@(0.8)];
    
    return result;
}

+ (bool) CompareTheResultByCompare:(NSString *)compare threshold:(NSNumber *)threshold value:(NSNumber *)value{
    bool result = false;
    if ([compare isEqualToString:@"equal_greater"]) {
        return value.doubleValue >= threshold.doubleValue;
    }else if ([compare isEqualToString:@"equal_lower"]) {
        result = value.doubleValue <= threshold.doubleValue;
        return result;
    }else if ([compare isEqualToString:@"greater"]) {
        return value.doubleValue > threshold.doubleValue;
    }else if ([compare isEqualToString:@"lower"]) {
        return value.doubleValue < threshold.doubleValue;
    }else{
        return value.doubleValue == threshold.doubleValue;
    }
}

+ (NSNumber *)HandleTheWeatherDataByFormula:(NSString *)formula data:(NSArray *)data{
    NSNumber *result = @(0.0);
    if (data.count == 0) {
        return @(-1e9);
    }
    if ([formula isEqualToString:@"singlepoint"]) {
        result = data[0];
    }else if ([formula isEqualToString:@"max"]) {
        NSNumber *max = @(-1e9);
        for (NSNumber * value in data) {
            if (max < value) {
                max = value;
            }
        }
        result = max;
    }else if ([formula isEqualToString:@"min"]) {
        NSNumber *min = @(1e9);
        for (NSNumber * value in data) {
            if (min > value) {
                min = value;
            }
        }
        result = min;
    }else if ([formula isEqualToString:@"peak2peak"] || [formula isEqualToString:@"decrease"]) {
        NSNumber *max = @(-1e9);
        NSNumber *min = @(1e9);
        for (NSNumber * value in data) {
            if (max < value) {
                max = value;
            }
            if (min > value) {
                min = value;
            }
        }
        result = @(max.doubleValue - min.doubleValue);
    }else if ([formula isEqualToString:@"average"]) {
        double mean = 0.0;
        NSInteger count = data.count;
        for (NSNumber * value in data) {
            mean += value.doubleValue / count;
        }
        result = @(mean);
    }else {
        result = @(0.0);
    }
    return result;
}

+ (NSArray *) GetWeatherData_RelativeHumidity:(NSNumber *) duration Offset:(NSNumber *) offset {
    NSMutableArray *result = [NSMutableArray array];
    return result;
}
+ (NSArray *) GetWeatherData_RelativeHumidity_RealTime {
    NSMutableArray *result = [NSMutableArray array];
    [result addObject:[[self getCurrenWeatherData].sensorValues objectForKey:@"relativeHumidity"]];
    return result;
}

+ (NSArray *) GetWeatherData_AmbientTemp:(NSNumber *) duration Offset:(NSNumber *) offset {
    NSMutableArray *result = [NSMutableArray array];
    
    return result;
}

+ (NSArray *) GetWeatherData_AmbientTemp_RealTime {
    NSMutableArray *result = [NSMutableArray array];
    [result addObject:[[self getCurrenWeatherData].sensorValues objectForKey:@"ambientTemp"]];
    return result;
}

+ (NSArray *) GetWeatherData_Pressure:(NSNumber *) duration Offset:(NSNumber *) offset {
    NSMutableArray *result = [NSMutableArray array];
    
    return result;
}

+ (NSArray *) GetWeatherData_UVI:(NSNumber *) duration Offset:(NSNumber *) offset {
    NSMutableArray *result = [NSMutableArray array];
    
    return result;
}
+ (NSArray *) GetWeatherData_UVI_RealTime{
    NSMutableArray *result = [NSMutableArray array];
    [result addObject:[[self getCurrenWeatherData].sensorValues objectForKey:@"UVI"]];


    return result;
}
+ (NSArray *) GetWeatherData_SensibleTemp:(NSNumber *) duration Offset:(NSNumber *) offset {
    NSMutableArray *result = [NSMutableArray array];
    
    return result;
}
+ (NSArray *) GetWeatherData_SensibleTemp_RealTime{
    NSMutableArray *result = [NSMutableArray array];
    [result addObject:[[self getCurrenWeatherData].sensorValues objectForKey:@"sendibleTemp"]];
    return result;
}


+ (NSArray *) GetWeatherData_OutdoorIndex:(NSNumber *) duration Offset:(NSNumber *) offset {
    NSMutableArray *result = [NSMutableArray array];
    
    return result;
}

+ (NSArray *) GetWeatherData_ComfortLevel:(NSNumber *) duration Offset:(NSNumber *) offset {
    NSMutableArray *result = [NSMutableArray array];
    
    return result;
}

+ (NSArray *) GetWeatherData_ComfortLevel_RealTime {
    NSMutableArray *result = [NSMutableArray array];
    [result addObject:[[self getCurrenWeatherData].sensorValues objectForKey:@"comfortLevel"]];
    return result;
}

+ (WLMonitorService *)getCurrenWeatherData{
    WLCentralManager * manager = [WLCentralManager sharedService];
    if (manager.ymsPeripherals.count != 0) {
        WLWeatherLog * weatherLog = manager.ymsPeripherals[0];
        WLMonitorService * monitorService =  weatherLog.serviceDict[@"monitor"];
        return monitorService;
    }
    return nil;
}

@end
