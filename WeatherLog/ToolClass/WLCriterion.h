//
//  WLCriterion.h
//  WeatherLog
//
//  Created by 龙恒舟 on 15/9/22.
//  Copyright © 2015年 龙恒舟. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WLCriterion : NSObject

@property (strong, nonatomic, readonly) NSString *notification;

@property (strong, nonatomic, readonly) NSNumber *triggeredat;

@property (strong, nonatomic, readonly) NSNumber *priority;

@property (nonatomic, assign, readonly) BOOL IsEffective;

//- (BOOL) IsEffective;

- (void) SetValue:(NSString *) thenotification triggeredat:(NSNumber *) thetriggeredat priority:(NSNumber *) thepriority IsEffective:(bool) effective;

@end
