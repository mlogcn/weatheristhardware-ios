//
//  NetWorkManager.h
//  WeatherLog
//
//  Created by ink on 15/9/1.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ACMsg.h"
#import "WeatherModel.h"
#import "ACObject.h"
#import "ACloudLib.h"
@interface NetWorkManager : NSObject
- (void)sentDataToService:(NSArray *)dataArray Result:(void (^)(ACMsg *responseMsg, NSError *error))result;
@end
