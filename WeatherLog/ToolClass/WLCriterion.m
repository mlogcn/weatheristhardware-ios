//
//  WLCriterion.m
//  WeatherLog
//
//  Created by 龙恒舟 on 15/9/22.
//  Copyright © 2015年 龙恒舟. All rights reserved.
//

#import "WLCriterion.h"

@interface WLCriterion ()

@property (nonatomic, assign) BOOL IsEffective;

@property (strong, nonatomic) NSString *notification;

@property (strong, nonatomic) NSNumber *triggeredat;

@property (strong, nonatomic) NSNumber *priority;

@end

@implementation WLCriterion

//- (BOOL) IsEffective {
//    return _IsEffective;
//}

- (void) SetValue:(NSString *) thenotification triggeredat:(NSNumber *) thetriggeredat priority:(NSNumber *) thepriority IsEffective:(bool) effective{
    _notification = thenotification;
    _priority = thepriority;
    _triggeredat = thetriggeredat;
    _IsEffective = effective?YES:NO;
}

@end
