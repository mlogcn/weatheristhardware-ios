//
//  WeatherDataBase.h
//  WeatherLog
//
//  Created by ink on 15/9/2.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FMDB.h>
@interface WeatherDataBase : NSObject
+ (FMDatabase *)setup;
+ (void) close;
@end
