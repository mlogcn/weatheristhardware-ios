//
//  WeatherModel.h
//  WeatherLog
//
//  Created by ink on 15/9/1.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WeatherModel : NSObject
/**
 *  用户ID
 */
@property (assign) NSInteger userID;
/**
 *  设备ID
 */
@property (nonatomic, copy) NSString * deviceID;
/**
 *  创建时间
 */
@property (assign) NSInteger createdTime;

@property (assign) NSInteger measuredTime;

@property (assign) float longitude;

@property (assign) float latitude;

@property (assign) float temperature;

@property (assign) float humidity;

@property (assign) float pressure;

@property (assign) float UVIndex;
@end
