 //
//  SetTextWithWeather.h
//  
//
//  Created by ink on 15/9/18.
//
//

#import <Foundation/Foundation.h>
#import "GDataXMLNode.h"
#import "WLCriterion.h"
#import "WLHandleWeatherData.h"
@interface SetTextWithWeather : NSObject

- (NSArray *)ParseTheXML;

- (bool)LoadTheXML;

@end
