//
//  WeatherDataBase.m
//  WeatherLog
//
//  Created by ink on 15/9/2.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import "WeatherDataBase.h"
static FMDatabase * dbPointer;
@implementation WeatherDataBase
+ (FMDatabase *)setup{
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * documentDirectory = [paths objectAtIndex:0];
    NSString * dbpath = [documentDirectory stringByAppendingPathComponent:@"Weather.db"];
    dbPointer = [FMDatabase databaseWithPath:dbpath];
    if (![dbPointer open]) {
        NSLog(@"打开失败");
        return 0;
    }
    return dbPointer;
}
+ (void)close{
    if (dbPointer) {
        [dbPointer close];
        dbPointer = NULL;
    }

}
@end
