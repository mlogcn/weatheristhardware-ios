//
//  SetTextWithWeather.m
//  
//
//  Created by ink on 15/9/18.
//
//

#import "SetTextWithWeather.h"



@interface SetTextWithWeather ()

@property (nonatomic, strong) GDataXMLDocument *mDoc;

@end

@implementation SetTextWithWeather


- (bool)LoadTheXML {
    NSString * xmlFile = [NSString stringWithContentsOfURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"WeatherLogNotification_20151012" ofType:@"xml"]] encoding:NSUTF8StringEncoding error:nil];
    _mDoc = [[GDataXMLDocument alloc] initWithXMLString:xmlFile options:0 error:nil];
    if (!_mDoc) {
        return false;
    }
    NSLog(@"+++++ Have loaded the xml file successfully!");
    return true;
}

- (NSArray *)ParseTheXML{
    GDataXMLDocument * doc = _mDoc;
    if (!doc) {
        return nil;
    }
    NSLog(@"+++++ Starting decode the xml file");
    
    NSMutableArray *arrayOfEffectiveCriterions = [NSMutableArray array];
    
    NSArray * nodes = doc.rootElement.children;
    for (GDataXMLElement *model in nodes) {
        if ([[model name] isEqualToString:@"comment"]) {
            NSLog(@"+++++ comment in root!");
            continue;
        }
        bool willBeUsed = false;     //临时变量，暂时只计算comfortLevel和senTemp两个变量
        
        NSArray *thesonsofvariables, *thesonsscriterions;
        NSString *thename, *thedescription;
        
        NSMutableArray *arrayOfVariables = [NSMutableArray array];
        
        NSArray *sonsOfModel = [model children];
        for (GDataXMLElement *sonOfModel in sonsOfModel) {
            if ([[sonOfModel name] isEqualToString:@"comment"]) {
                NSLog(@"+++++ comment in model!");
                continue;
            }
            
            if ([[sonOfModel name] isEqualToString:@"name"]) {
                thename = [sonOfModel stringValue];
                NSLog(@"----The name of this model is: %@", thename);
                if ([thename isEqualToString:@"comfortLevel"] || [thename isEqualToString:@"senTemp"]) {
                    willBeUsed = true;
                }else{
                    willBeUsed = false;
                }
            }else if ([[sonOfModel name] isEqualToString:@"description"]) {
                thedescription = [sonOfModel stringValue];
                NSLog(@"----The description of this model is: %@", thedescription);
            }else if ([[sonOfModel name] isEqualToString:@"variables"]) {
                thesonsofvariables = [sonOfModel children];
                for (GDataXMLElement *eleOfVariable in thesonsofvariables) {
                    if ([[eleOfVariable name] isEqualToString:@"comment"]) {
                        NSLog(@"+++++ comment in variables!");
                        continue;
                    }
                    
                    NSString *tempVariable = [eleOfVariable stringValue];
                    [arrayOfVariables addObject:tempVariable];
                    NSLog(@"+++++ the model contains variable: %@", tempVariable);
                }
            }else {
                //the criterions
                thesonsscriterions = [sonOfModel children];
                int i = 0;
                NSLog(@"+++++ the model contains %lu criterions", thesonsscriterions.count);
                
                for (GDataXMLElement *eleOfCriterion in thesonsscriterions) {
                    if ([[eleOfCriterion name] isEqualToString:@"comment"]) {
                        NSLog(@"+++++ comment in criterions!");
                        continue;
                    }
                    if ([[eleOfCriterion name] isEqualToString:@"criterion"]) {
                        NSLog(@"+++++ starting decode the %d criterion", i);
                        
                        WLCriterion *criterion = [self ParseCriterion:eleOfCriterion];
                        bool tempFlag = criterion.IsEffective;
                        if (tempFlag) {
                            NSLog(@"+++++ Add the criterion into the effect array!");
                            if (willBeUsed) {
                                [arrayOfEffectiveCriterions addObject:criterion];
                            }
                        }
                    }
                    i++;
                }
            }
        }
    }
//    NSLog(@"The count of effective criterions is: %lu", arrayOfEffectiveCriterions.count);
//    if (arrayOfEffectiveCriterions.count != 0) {
//        WLCriterion * criterion = arrayOfEffectiveCriterions[0];
//        for (WLCriterion * criterionTmp in arrayOfEffectiveCriterions) {
//            if (criterionTmp.priority > criterion.priority) {
//                criterion = criterionTmp;
//            }
//        }
//        return criterion;
//    }
    return arrayOfEffectiveCriterions;
    
    //下面可以根据优先级得到要显示的criterion，然后再处理
}

- (WLCriterion *) ParseCriterion:(GDataXMLElement *)rootEle{
    NSString *thenotification;
    NSNumber *thepriority;
    NSNumber *thetriggeredat;
    
    NSArray *sonsOfCriterion = [rootEle children];
    
    NSMutableArray *arrayOfExpressions = [NSMutableArray array];
    NSMutableArray *arrayOfRelation = [NSMutableArray array];
    
    for (GDataXMLElement *sonOfCriterion in sonsOfCriterion) {
        if ([[sonOfCriterion name] isEqualToString:@"comment"]) {
            NSLog(@"+++++ comment in criterion!");
            continue;
        }
        
        if ([[sonOfCriterion name] isEqualToString:@"notification"]) {
            thenotification = [NSString stringWithString:[sonOfCriterion stringValue]];
        }else if ([[sonOfCriterion name] isEqualToString:@"priority"]) {
            thepriority = [NSNumber numberWithDouble:[[sonOfCriterion stringValue] doubleValue]];
        }else if ([[sonOfCriterion name] isEqualToString:@"triggeredat"]) {
            thetriggeredat = [NSNumber numberWithDouble:[[sonOfCriterion stringValue] doubleValue]];
        }else if ([[sonOfCriterion name] isEqualToString:@"relation"]) {
            //garther relations
            [arrayOfRelation addObject:sonOfCriterion];
        }else {
            //garther expressions
            [arrayOfExpressions addObject:sonOfCriterion];
        }
    }
    
    //decode the expression
    bool bResult = [self ParseExpression:arrayOfExpressions[0]];
    
    if ((arrayOfRelation.count > 0) && (arrayOfExpressions.count == arrayOfRelation.count + 1)) {
        for (int i = 0; i < arrayOfRelation.count; i++) {
            GDataXMLElement *eleRelation = arrayOfRelation[i];
            GDataXMLElement *eleExpression = arrayOfExpressions[i+1];
            bool tempResult = [self ParseExpression:eleExpression];
            NSString *tempString = [eleRelation stringValue];
            if ([tempString isEqualToString:@"or"]) {
                bResult = bResult || tempResult;
            }else if ([tempString isEqualToString:@"and"]) {
                bResult = bResult && tempResult;
            }else {
                bResult = (bResult && !tempResult) || (!bResult && tempResult);
            }
        }
    }
    
    WLCriterion *result = [WLCriterion new];
    [result SetValue:thenotification triggeredat:thetriggeredat priority:thepriority IsEffective:bResult];
    return result;
}

- (bool) ParseExpression:(GDataXMLElement *)rootEle {
    
    NSNumber *d_duration;
    NSNumber *d_offset;
    NSNumber *d_threshhold;
    NSString *str_compare;
    NSString *str_variable;
    NSString *str_formula;
    
    NSArray *sonsOfExpression = [rootEle children];
    for (GDataXMLElement *sonOfExpression in sonsOfExpression) {
        if ([[sonOfExpression name] isEqualToString:@"comment"]) {
            NSLog(@"+++++ comment in expression!");
            continue;
        }
        
        if ([[sonOfExpression name] isEqualToString:@"variable"]) {
            
            str_variable = [sonOfExpression stringValue];
            NSLog(@"+++++ The variable of expression is %@", str_variable);
            
        }else if ([[sonOfExpression name] isEqualToString:@"duration"]) {
            
            NSArray *tempArray = [sonOfExpression children];
            for (GDataXMLElement *sonOfDuration in tempArray) {
                if ([[sonOfDuration name] isEqualToString:@"comment"]) {
                    NSLog(@"+++++ comment in duration!");
                    continue;
                }else if ([[sonOfDuration name] isEqualToString:@"text"]){
                    d_duration = @([[sonOfDuration stringValue] doubleValue]);
                    NSLog(@"+++++ The duration is %f", d_duration.doubleValue);
                    
                }else if ([[sonOfDuration name] isEqualToString:@"offset"]){
                    d_offset = @([[sonOfDuration stringValue] doubleValue]);
                    NSLog(@"The offset is %f", d_offset.doubleValue);
                }
            }
            
        }else if ([[sonOfExpression name] isEqualToString:@"compare"]) {
            
            NSArray *tempArray = [sonOfExpression children];
            for (GDataXMLElement *sonOfCompare in tempArray) {
                if ([[sonOfCompare name] isEqualToString:@"comment"]) {
                    NSLog(@"+++++ comment in compare!");
                    continue;
                }else if ([[sonOfCompare name] isEqualToString:@"text"]){
                    str_compare = [sonOfCompare stringValue];
                    
                }else if ([[sonOfCompare name] isEqualToString:@"threshold"]){
                    d_threshhold = @([[sonOfCompare stringValue] doubleValue]);
                }
            }
        }else {
            //formula
            str_formula = [sonOfExpression stringValue];
            
        }
    }
    NSLog(@"+++ starting fetch the data!");
    NSArray *arrayOfData = [WLHandleWeatherData GetTheWeatherDataByName:str_variable Duration:d_duration Offset:d_offset];
    NSLog(@"++++  %@ records have been gathered!", @(arrayOfData.count));
    
    
    NSNumber *value = [WLHandleWeatherData HandleTheWeatherDataByFormula:str_formula data:arrayOfData];
    if (value.integerValue == -1e9) {
        return false;
    }
    NSLog(@"++++ The value is %@", value);
    NSLog(@"+++++ The compare is %@", str_compare);
    NSLog(@"++++ The formula is %@", str_formula);
    NSLog(@"The threshold is %f", d_threshhold.doubleValue);
    
    bool result = [WLHandleWeatherData CompareTheResultByCompare:str_compare threshold:d_threshhold value:value];
    
    return result;
}

@end
