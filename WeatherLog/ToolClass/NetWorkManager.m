//
//  NetWorkManager.m
//  WeatherLog
//
//  Created by ink on 15/9/1.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import "NetWorkManager.h"

@implementation NetWorkManager
- (void)sentDataToService:(NSArray *)dataArray
                   Result:(void (^)(ACMsg *, NSError *))result{
    
    
    ACMsg *msg = [[ACMsg alloc] init];
    msg.context = [ACContext generateContextWithSubDomain:@"weatherlog_alpha"];
    msg.name = @"storeDataArray";

    for (WeatherModel * model in dataArray) {
        ACObject * obj = [ACObject new];
        [obj putLong:@"userID" value:(long)model.userID];
        [obj putString:@"deviceID" value:model.deviceID];
        [obj putLong:@"createdTime" value:(long)model.createdTime];
        [obj putLong:@"measuredTime" value:(long)[[NSDate date] timeIntervalSince1970]*1000];
        [obj putFloat:@"longitude" value:model.longitude];
        [obj putFloat:@"latitude" value:model.latitude];
        [obj putFloat:@"temperature" value:model.temperature];
        [obj putFloat:@"humidity" value:model.humidity];
        [obj putFloat:@"pressure" value:model.pressure];
        [obj putFloat:@"UVIndex" value:model.UVIndex];
        [msg addACObject:@"value" value:obj];
        
    }
    [ACloudLib sendToService:@"weatherlog_alpha"
                 serviceName:@"WeatherLog"
                     version:1
                         msg:msg
                    callback:^(ACMsg *responseMsg, NSError *error) {
                        result(responseMsg,error);
                    }];
    
}
@end
