//
//  WLFetchWeatherData.h
//  WeatherLog
//
//  Created by 龙恒舟 on 15/9/22.
//  Copyright © 2015年 龙恒舟. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WLCentralManager.h"
#import "WLWeatherLog.h"
#import "WLMonitorService.h"
@interface WLHandleWeatherData : NSObject

+ (NSArray *) GetTheWeatherDataByName:(NSString *) name Duration:(NSNumber *) duration Offset:(NSNumber *) offset;

+ (bool) CompareTheResultByCompare:(NSString *)compare threshold:(NSNumber *)threshold value:(NSNumber *)value;

+ (NSNumber *) HandleTheWeatherDataByFormula:(NSString *)formula data:(NSArray *)data;
@end
