//
//  RootViewModel.m
//  WeatherLog
//
//  Created by ink on 15/9/8.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import "RootViewModel.h"

@implementation RootViewModel
#pragma makr - Public Methods
- (RACSignal *)fetchNetData{
    return [[WeatheristAPIManager shareManager] fetchWeatherData];
}


#pragma mark Accessors
- (NSDictionary *)dataDic{
    
    if (!_dataDic) {
        _dataDic = [NSDictionary dictionary];
       
    }
     return _dataDic;
}

@end
