//
//  RootViewModel.h
//  WeatherLog
//
//  Created by ink on 15/9/8.
//  Copyright (c) 2015年 龙恒舟. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RACSignal.h>
#import "WeatheristAPIManager.h"
@interface RootViewModel : NSObject

@property (nonatomic) NSDictionary * dataDic;

- (RACSignal *)fetchNetData;

@end
